﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiException : Exception
    {
        public HuobiException(HuobiErrorCode code, string msg) : base(msg) { }
    }
}
