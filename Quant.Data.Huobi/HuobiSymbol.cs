﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiSymbol
    {
        /// <summary>
        /// 交易对中的基础币种
        /// </summary>
        [JsonProperty("base-currency")]
        public string BaseCurrency { get; private set; }

        /// <summary>
        /// 交易对中的报价币种
        /// </summary>
        [JsonProperty("quote-currency")]
        public string QuoteCurrency { get; private set; }

        /// <summary>
        /// 交易对报价的精度（小数点后位数）
        /// </summary>
        [JsonProperty("price-precision")]
        public int PricePrecision { get; private set; }

        /// <summary>
        /// 交易对基础币种计数精度（小数点后位数）
        /// </summary>
        [JsonProperty("amount-precision")]
        public int AmountPrecision { get; private set; }

        /// <summary>
        /// 交易区，可能值: [main，innovation]
        /// </summary>
        [JsonProperty("symbol-partition")]
        public HuobiSymbolPartition SymbolPartition { get; private set; }

        /// <summary>
        /// 交易对
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; private set; }

        /// <summary>
        /// 交易对状态
        /// </summary>
        [JsonProperty("state")]
        public HuobiSymbolState State { get; private set; }

        /// <summary>
        /// 交易对交易金额的精度（小数点后位数）
        /// </summary>
        [JsonProperty("value-precision")]
        public int ValuePrecision { get; private set; }

        /// <summary>
        /// 交易对最小下单量 (下单量指当订单类型为限价单或sell-market时，下单接口传的'amount')
        /// </summary>
        [JsonProperty("min-order-amt")]
        public float MinOrderAmount { get; private set; }

        /// <summary>
        /// 交易对最大下单量
        /// </summary>
        [JsonProperty("max-order-amt")]
        public float MaxOrderAmount { get; private set; }

        /// <summary>
        /// 最小下单金额 （下单金额指当订单类型为限价单时，下单接口传入的(amount * price)。当订单类型为buy-market时，下单接口传的'amount'）
        /// </summary>
        [JsonProperty("min-order-value")]
        public float MinOrderValue { get; private set; }

        /// <summary>
        /// 交易对杠杆最大倍数
        /// </summary>
        [JsonProperty("leverage-ratio")]
        public float? LeverageRatio { get; private set; }
    }
}

