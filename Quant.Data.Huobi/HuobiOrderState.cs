﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiOrderStateConverter))]
    public enum HuobiOrderState
    {
        /// <summary>
        /// 正在提交
        /// </summary>
        Submitting,
        /// <summary>
        /// 已提交
        /// </summary>
        Submitted,
        /// <summary>
        /// 部分成交
        /// </summary>
        PartialFilled,
        /// <summary>
        /// 部分成交撤销
        /// </summary>
        PartialCanceled,
        /// <summary>
        /// 完全成交
        /// </summary>
        Filled,
        /// <summary>
        /// 正在撤销
        /// </summary>
        Cancelling,
        /// <summary>
        /// 已撤销
        /// </summary>
        Canceled,
    }

    internal static class HuobiOrderStateExtenstions {
        public static readonly IReadOnlyDictionary<HuobiOrderState, string> Nicknames = new Dictionary<HuobiOrderState, string>() {
            { HuobiOrderState.Submitting, "submitting" },
            { HuobiOrderState.Submitted, "submitted" },
            { HuobiOrderState.PartialFilled, "partial-filled" },
            { HuobiOrderState.PartialCanceled, "partial-canceled" },
            { HuobiOrderState.Filled, "filled" },
            { HuobiOrderState.Cancelling, "cancelling" },
            { HuobiOrderState.Canceled, "canceled" },
        };

        public static string GetNickname(this HuobiOrderState state)
        {
            return Nicknames[state];
        }
    }

    internal class HuobiOrderStateConverter : CustomStringEnumConverter<HuobiOrderState>
    {
        public HuobiOrderStateConverter() : base(HuobiOrderStateExtenstions.Nicknames)
        { }
    }
}
