﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiErrorCodeConverter))]
    public enum HuobiErrorCode
    {
        None = 0,
        BadRequest = 1,
        InvalidParameter = 2,
        InvalidCommand = 3,
        /// <summary>
        /// 账户余额不足
        /// </summary>
        AccountTransferBalanceInsufficientError = 4,
        /// <summary>
        /// 禁止操作（母子账号关系错误时报）
        /// </summary>
        BaseOperationForbidden = 5,
    }

    internal class HuobiErrorCodeConverter : CustomStringEnumConverter<HuobiErrorCode>
    {
        public HuobiErrorCodeConverter() : base(new Dictionary<HuobiErrorCode, string>() {
            { HuobiErrorCode.None, "none" },
            { HuobiErrorCode.BadRequest, "bad_request" },
            { HuobiErrorCode.InvalidParameter, "invalid_parameter" },
            { HuobiErrorCode.InvalidCommand, "invalid_command" },
            { HuobiErrorCode.AccountTransferBalanceInsufficientError, "account-transfer-balance-insufficient-error" },
            { HuobiErrorCode.BaseOperationForbidden, "base-operation-forbidden" },
        })
        { }
    }
}
