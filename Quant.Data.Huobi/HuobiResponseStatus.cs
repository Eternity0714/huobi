﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiResponseStatusConverter))]
    public enum HuobiResponseStatus
    {
        OK = 1,
        Error = 2,
    }

    internal class HuobiResponseStatusConverter : CustomStringEnumConverter<HuobiResponseStatus>
    {
        public HuobiResponseStatusConverter() : base(new Dictionary<HuobiResponseStatus, string>() {
            { HuobiResponseStatus.OK, "ok" },
            { HuobiResponseStatus.Error, "error" },
        }) {

        }
    }
}
