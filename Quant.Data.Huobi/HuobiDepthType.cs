﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public enum HuobiDepthType
    {
        Default = 0,
        Five = 5,
        Ten = 10,
        Twenty = 20,
    }
}
