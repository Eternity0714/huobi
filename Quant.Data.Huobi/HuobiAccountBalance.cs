﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiAccountBalance : HuobiAccount
    {
        [JsonProperty("list")]
        public IList<HuobiBalance> Balances { get; internal set; }
    }
}
