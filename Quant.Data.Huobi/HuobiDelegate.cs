﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiDelegateConverter))]
    public class HuobiDelegate
    {
        public double Price { get; internal set; }

        public double Volume { get; internal set; }
    }

    internal class HuobiDelegateConverter : JsonConverter<HuobiDelegate>
    {
        public override HuobiDelegate ReadJson(JsonReader reader, Type objectType, HuobiDelegate existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (!hasExistingValue) {
                existingValue = new HuobiDelegate();
            }

            if (reader.TokenType == JsonToken.StartArray)
            {
                var list = serializer.Deserialize<List<double>>(reader);
                existingValue.Price = list[0];
                existingValue.Volume = list[1];

                return existingValue;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public override void WriteJson(JsonWriter writer, HuobiDelegate value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
