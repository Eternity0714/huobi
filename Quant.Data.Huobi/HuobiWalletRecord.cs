﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public abstract class HuobiWalletRecord
    {
        //[JsonProperty("id")]
        //public long ID { get; internal set; }

        ///// <summary>
        ///// 币种
        ///// </summary>
        //[JsonProperty("currency")]
        //public string Currency { get; internal set; }

        ///// <summary>
        ///// 交易哈希
        ///// </summary>
        //[JsonProperty("tx-hash")]
        //public string TransactionHash { get; internal set; }

        ///// <summary>
        ///// 个数
        ///// </summary>
        //[JsonProperty("amount")]
        //public double Amount { get; internal set; }

        //public 
    }

    /// <summary>
    /// 充值记录
    /// </summary>
    public class HuobiWalletDepositRecord : HuobiWalletRecord {

    }

    /// <summary>
    /// 提现记录
    /// </summary>
    public class HuobiWalletWithdrawRecord : HuobiWalletRecord {

    }
}
