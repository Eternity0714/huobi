﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 深度的价格聚合度
    /// </summary>
    [JsonConverter(typeof(HuobiDepthStepConverter))]
    public enum HuobiDepthStep
    {
        /// <summary>
        /// 无聚合
        /// </summary>
        Zero = 0,
        /// <summary>
        /// 聚合度为报价精度*10
        /// </summary>
        One = 1,
        /// <summary>
        /// 聚合度为报价精度*100
        /// </summary>
        Two = 2,
        /// <summary>
        /// 聚合度为报价精度*1000
        /// </summary>
        Three = 3,
        /// <summary>
        /// 聚合度为报价精度*10000
        /// </summary>
        Four = 4,
        /// <summary>
        /// 聚合度为报价精度*100000
        /// </summary>
        Five = 5,
    }

    internal static class HuobiDepthStepExtensions
    {
        public static readonly IReadOnlyDictionary<HuobiDepthStep, string> Nicknames = new Dictionary<HuobiDepthStep, string>() {
            { HuobiDepthStep.Zero, "step0" },
            { HuobiDepthStep.One, "step1" },
            { HuobiDepthStep.Two, "step2" },
            { HuobiDepthStep.Three, "step3" },
            { HuobiDepthStep.Four, "step4" },
            { HuobiDepthStep.Five, "step5" },
        };

        public static string GetNickname(this HuobiDepthStep step)
        {
            return Nicknames[step];
        }
    }

    internal class HuobiDepthStepConverter : CustomStringEnumConverter<HuobiDepthStep> {
        public HuobiDepthStepConverter() : base(HuobiDepthStepExtensions.Nicknames) { }
    }
}
