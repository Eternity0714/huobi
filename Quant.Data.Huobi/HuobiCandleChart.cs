﻿using Quant.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiCandleChart : CandleChart
    {
        public long id;

        public override DateTime OpenTime
        {
            get
            {
                return Global.UnixEpoch.AddSeconds(id).ToLocalTime();
            }
        }
    }
}

