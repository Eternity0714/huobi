﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiOrderDirectionConverter))]
    public enum HuobiOrderDirection
    {
        Buy = 1,
        Sell = 2,
    }

    internal static class HuobiOrderDirectionExtensions {
        public static readonly IReadOnlyDictionary<HuobiOrderDirection, string> Nicknames = new Dictionary<HuobiOrderDirection, string>() {
            { HuobiOrderDirection.Buy, "buy" },
            { HuobiOrderDirection.Sell, "sell" },
        };

        public static string GetNickname(this HuobiOrderDirection direction)
        {
            return Nicknames[direction];
        }
    }

    internal class HuobiOrderDirectionConverter : CustomStringEnumConverter<HuobiOrderDirection>
    {
        public HuobiOrderDirectionConverter() : base(HuobiOrderDirectionExtensions.Nicknames)
        { }
    }
}
