﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    internal class HuobiError
    {
        [JsonProperty("code")]
        public HuobiErrorCode ErrorCode { get; private set; }

        [JsonProperty("err-msg")]
        public string Message { get; private set; }
    }
}
