﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Quant.Core;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 稳定币兑换汇率
    /// </summary>
    public class HuobiStableCoinRate
    {
        /// <summary>
        /// 稳定币币种
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; internal set; }

        /// <summary>
        /// 兑入汇率，即用户方使用HUSD买入查询稳定币的汇率
        /// </summary>
        [JsonProperty("buy-rate")]
        public double BuyRate { get; internal set; }

        /// <summary>
        /// 兑入限额，即当前平台支持用户方可以买入的稳定币最大数量
        /// </summary>
        [JsonProperty("buy_quota")]
        public double BuyQuota { get; internal set; }

        /// <summary>
        /// 兑出汇率，即用户方使用查询稳定币买入HUSD的汇率
        /// </summary>
        [JsonProperty("sell_rate")]
        public double SellRate { get; internal set; }

        /// <summary>
        /// 兑出限额，即当前平台支持用户方可以卖出的稳定币最大数量
        /// </summary>
        [JsonProperty("sell_quota")]
        public double SellQuota { get; internal set; }

        /// <summary>
        /// 稳定币兑入兑出状态
        /// </summary>
        [JsonProperty("state")]
        public HuobiStableCoinExchangeState ExchangeState { get; internal set; }

        [JsonProperty("time")]
        private long _ts;

        /// <summary>
        /// 时间戳
        /// </summary>
        [JsonIgnore]
        public DateTime Time {
            get {
                return Global.UnixEpoch.AddMilliseconds(_ts).ToLocalTime();
            }
        }
    }
}
