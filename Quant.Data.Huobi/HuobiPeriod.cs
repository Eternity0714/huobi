﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiPeriodConverter))]
    public enum HuobiPeriod
    {
        Minute_1,
        Minute_5,
        Minute_15,
        Minute_30,
        Minute_60,
        Day_1,
        Week_1,
        Month_1,
        Year_1,
    }

    internal static class HuobiPeriodHelper {
        public static readonly IReadOnlyDictionary<HuobiPeriod, string> descriptions = new Dictionary<HuobiPeriod, string>() {
            { HuobiPeriod.Minute_1, "1min" },
            { HuobiPeriod.Minute_5, "5min" },
            { HuobiPeriod.Minute_15, "15min" },
            { HuobiPeriod.Minute_30, "30min" },
            { HuobiPeriod.Minute_60, "60min" },
            { HuobiPeriod.Day_1, "1day" },
            { HuobiPeriod.Week_1, "1week" },
            { HuobiPeriod.Month_1, "1mon" },
            { HuobiPeriod.Year_1, "1year" },
        };

        public static string GetDescription(this HuobiPeriod period) {
            return descriptions[period];
        }
    }

    internal class HuobiPeriodConverter : CustomStringEnumConverter<HuobiPeriod> {
        public HuobiPeriodConverter() : base(HuobiPeriodHelper.descriptions) {

        }
    }
}
