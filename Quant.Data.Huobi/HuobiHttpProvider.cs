﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.WebSockets;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quant.Core;

namespace Quant.Data.Huobi
{
    public sealed class HuobiHttpProvider
    {
        public const string ACCESS_KEY = "73eb80e5-22dd9a3d-735f9950-mn8ikls4qg";
        public const string SECRET_KEY = "b0054e52-5bd496f9-bf4cd59f-765f9";

        const string HUOBI_HOST = "api.huobi.pro";
        static readonly string HUOBI_HOST_URL = $@"https://{HUOBI_HOST}";
        const string WsUrl = @"wss://api.huobi.pro/ws";
        const string WsV1Url = @"wss://api.huobi.pro/ws/v1";

        const string HUOBI_SIGNATURE_VERSION = "2";
        const string HUOBI_SIGNATURE_METHOD = "HmacSHA256";

        private HttpClient httpClient = new HttpClient();
        private ClientWebSocket websocket = new ClientWebSocket();

        private HuobiApiKey apikey = null;

        public HuobiHttpProvider(HuobiApiKey apikey)
        {
            this.apikey = apikey;
        }

        public HuobiHttpProvider(string accessKey, string secretKey) : this(new HuobiApiKey(accessKey, secretKey)) { }

        #region HuoBiApi方法

        #region 基础信息
        /// <summary>
        /// 获取所有交易对
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiSymbol>> GetSymbolsAsync() {
            const string route = @"/v1/common/symbols";
            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings);
            var data = response.Data.ToObject<List<HuobiSymbol>>();
            return data;
        }

        /// <summary>
        /// 获取所有币种
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<string>> GetCurrenciesAsync() {
            const string route = @"/v1/common/currencys";
            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings);
            var data = response.Data.ToObject<List<string>>();
            return data;
        }

        /// <summary>
        /// 获取当前系统时间(local)
        /// </summary>
        /// <returns></returns>
        public async Task<DateTime> GetTimestampAsync() {
            const string route = @"/v1/common/timestamp";
            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings);
            var value = response.Data.ToObject<long>();
            var data = Global.UnixEpoch.AddMilliseconds(value).ToLocalTime();
            return data;
        }

        #endregion 基础信息

        #region 行情数据

        /// <summary>
        /// 此接口返回历史K线数据。
        /// <para>获取 hb10 净值时， symbol 请填写 “hb10”。</para>
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="period"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<HuobiCandleChart[]> GetKLineAsync(string symbol, HuobiPeriod period, int size = 150) {
            if (size < 1 || size > 2000) throw new ArgumentException("Parameter \"size\" should be greater than or equal to 1 and less than or equal to 2000");

            Console.WriteLine("正在请求KLine数据");

            const string route = @"/market/history/kline";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "period", period.GetDescription() },
                { "size", size.ToString() },
                { "symbol", symbol },
            };

            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<HuobiCandleChart[]>();

            Console.WriteLine("正在请求KLine数据");

            return data.Reverse().ToArray();
        }

        /// <summary>
        /// 聚合行情（Ticker）
        /// <para>此接口获取ticker信息同时提供最近24小时的交易聚合信息。</para>
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        //public async Task<HuobiMergedTicker> GetMergedDetailAsync(string symbol) {
        //    const string route = @"/market/detail/merged";
        //    Dictionary<string, string> parameters = new Dictionary<string, string>() {
        //        { "symbol", symbol },
        //    };

        //    HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings, parameters);

        //    DateTime opentime = response.Timestemp.AddDays(-1);

        //    HuobiMergedTicker ticker = new HuobiMergedTicker()
        //    {
        //        Symbol = symbol,
        //        OpenTime = opentime,
        //        Period = HuobiPeriod.Day_1,
        //    };

        //    JsonSerializer serializer = JsonSerializer.Create();
        //    serializer.Populate(response.Data.CreateReader(), ticker);
        //    return ticker;
        //}

        /// <summary>
        /// 市场深度数据
        /// <para>此接口返回指定交易对的当前市场深度数据。</para>
        /// <para>当type值为‘step0’时，‘depth’的默认值为150而非20。</para>
        /// <para>返回的JSON顶级数据对象名为'tick'而不是通常的'data'。</para>
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="depthtype">返回深度的数量</param>
        /// <param name="step">	深度的价格聚合度</param>
        /// <returns></returns>
        public async Task<HuobiDepth> GetDepthsAsync(string symbol, HuobiDepthType depthtype = HuobiDepthType.Default, HuobiDepthStep step = HuobiDepthStep.Zero)
        {
            const string route = @"/market/depth";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "type", HuobiDepthStepExtensions.GetNickname(step) },
            };

            if (depthtype != HuobiDepthType.Default)
            {
                parameters.Add("depth", depthtype.GetHashCode().ToString());
            }

            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<HuobiDepth>();

            return data;
        }

        /// <summary>
        /// 获得近期交易记录
        /// <para>此接口返回指定交易对近期的所有交易记录。</para>
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="size">返回的交易记录数量，最大值2000</param>
        /// <returns></returns>
        public async Task<HuobiTrade[]> GetTradesAsync(string symbol, int size = 1) {
            if (size < 1 || size > 2000) throw new ArgumentException("Parameter \"size\" should be greater than or equal to 1 and less than or equal to 2000");

            const string route = @"market/history/trade";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "size", size.ToString()},
            };

            HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<HuobiTrade[]>();
            return data;
        }

        /// <summary>
        /// 最近24小时行情数据
        /// <para>此接口返回最近24小时的行情数据汇总。</para>
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <returns></returns>
        //public async Task<HuobiCandleChart> Get24HourDetailAsync(string symbol)
        //{
        //    const string route = @"/market/detail";
        //    Dictionary<string, string> parameters = new Dictionary<string, string>() {
        //        { "symbol", symbol },
        //    };

        //    HuobiResponse response = await SendGetRequestWithoutSignature(route, HuobiResponse.DataSettings, parameters);

        //    DateTime opentime = response.Timestemp.AddDays(-1);
        //    HuobiCandleChart kline = new HuobiCandleChart()
        //    {
        //        Symbol = symbol,
        //        OpenTime = opentime,
        //        Period = HuobiPeriod.Day_1,
        //    };
        //    JsonSerializer serializer = JsonSerializer.Create();
        //    serializer.Populate(response.Data.CreateReader(), kline);
        //    return kline;
        //}

        #endregion 行情数据

        #region 账户相关

        /// <summary>
        /// 查询当前用户的所有账户 ID account-id 及其相关信息
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiAccount>> GetAccountsAsync()
        {
            const string route = @"/v1/account/accounts";

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings);

            List<HuobiAccount> accounts = response.Data.ToObject<List<HuobiAccount>>();
            return accounts;
        }

        /// <summary>
        /// 查询指定账户的余额
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiBalance>> GetAccountBalanceAsync(HuobiAccount account)
        {
            string route = $@"/v1/account/accounts/{account.ID}/balance";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "account-id", account.ID.ToString() },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.Value<List<HuobiBalance>>("list");

            return data;
        }

        /// <summary>
        /// 母账户执行母子账号之间的划转
        /// </summary>
        /// <param name="subuid">子账号uid</param>
        /// <param name="currency">币种</param>
        /// <param name="amount">划转金额</param>
        /// <param name="transfertype">划转类型</param>
        /// <returns>划转订单id</returns>
        public async Task<long> SubuserTransferAsync(string subuid, string currency, double amount, HuobiTransferType transfertype)
        {
            const string route = @"/v1/subuser/transfer";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "sub-uid", subuid },
                { "currency", currency },
                { "amount", amount.ToString() },
                { "type", HuobiTransferTypeExtensions.GetNickname(transfertype) },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var orderid = response.Data.ToObject<long>();
            return orderid;
        }

        /// <summary>
        /// 母账户查询其下所有子账号的各币种汇总余额
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiBalance>> GetAggregateBalanceAsync()
        {
            const string route = @"/v1/subuser/aggregate-balance";

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings);

            List<HuobiBalance> data = response.Data.ToObject<List<HuobiBalance>>();

            return data;
        }

        /// <summary>
        /// 子账号余额
        /// 母账户查询子账号各币种账户余额
        /// </summary>
        /// <param name="subuid">子用户的 UID</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiAccountBalance>> GetSubBalanceAsync(string subuid)
        {
            string route = $@"/v1/account/accounts/{subuid}";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "sub-uid", subuid },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            List<HuobiAccountBalance> data = response.Data.ToObject<List<HuobiAccountBalance>>();

            return data;
        }

        #endregion 账户相关

        #region 稳定币兑换

        /// <summary>
        /// 查询稳定币兑换汇率
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiStableCoinRate>> GetStableCoinExchangeRateAsync() {
            string route = $@"/v1/stable_coin/exchange_rate";
            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings);

            List<HuobiStableCoinRate> data = response.Data.ToObject<List<HuobiStableCoinRate>>();

            return data;
        }

        /// <summary>
        /// 稳定币兑换
        /// </summary>
        /// <param name="currency">稳定币币种</param>
        /// <param name="amount">兑入或兑出的稳定币金额数量</param>
        /// <param name="direction">兑换方向, buy兑入/sell兑出</param>
        /// <returns></returns>
        public async Task<HuobiStableCoinExchange> ExchangeStableCoinAsync(string currency, double amount, HuobiOrderDirection direction) {
            const string route = @"/v1/stable_coin/exchange";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "currency", currency },
                { "amount", amount.ToString() },
                { "type", HuobiOrderDirectionExtensions.GetNickname(direction) },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<HuobiStableCoinExchange>();

            return data;
        }

        #endregion 稳定币兑换

        #region 钱包(充值与提现)

        /// <summary>
        /// 虚拟币提现
        /// <para>如果用户在 个人设置(https://www.hbg.com/zh-cn/user_center/uc_setting/) 里设置了优先使用快速提币，通过API发起的提币也会优先选择快速提币通道。快速提币是指当提币目标地址是火币用户地址时，提币将通过火币平台内部快速通道，不通过区块链。</para>
        /// <para>API提币仅支持用户的相应币种 常用地址列表(https://www.hbg.com/zh-cn/withdraw_address/) 中的地址。</para>
        /// </summary>
        /// <param name="address">提现地址, 仅支持在官网上相应币种 地址列表(https://www.hbg.com/zh-cn/withdraw_address/) 中的地址</param>
        /// <param name="amount">提币数量</param>
        /// <param name="currency">资产类型</param>
        /// <param name="fee">转账手续费</param>
        /// <param name="chain">提 USDT-ERC20 时需要设置此参数为 "usdterc20"，其他币种提现不需要设置此参数</param>
        /// <param name="addrtag">虚拟币共享地址tag，适用于xrp，xem，bts，steem，eos，xmr</param>
        /// <returns>提现 ID</returns>
        public async Task<long> WithdrawCreateAsync(string address, double amount, string currency, double? fee, string chain = null, string addrtag = null) {
            const string route = @"/v1/dw/withdraw/api/create";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "address", address },
                { "amount", amount.ToString() },
                { "currency", currency },
            };

            if (fee.HasValue) parameters.Add("fee", fee.Value.ToString());
            if (!chain.IsNullOrEmpty()) parameters.Add("chain", chain);
            if (!addrtag.IsNullOrEmpty()) parameters.Add("addr-tag", addrtag);

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 取消提现
        /// </summary>
        /// <param name="withdrawid">提现 ID，填在 path 中</param>
        /// <returns>提现 ID</returns>
        public async Task<long> WithdrawCancelAsync(long withdrawid) {
            string route = $@"/v1/dw/withdraw-virtual/{withdrawid}/cancel";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "withdraw-id", withdrawid.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 充值记录
        /// https://huobiapi.github.io/docs/spot/v1/cn/#d3612d98d4
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="withdrawid"></param>
        /// <param name="size"></param>
        /// <param name="direct"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiWalletWithdrawRecord>> GetWithdrawRecordsAsync(string currency = null, long? withdrawid = null, int size = 100, HuobiQueryDirect direct = HuobiQueryDirect.Prev) {
            string route = $@"/v1/query/deposit-withdraw";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "type", "withdraw" },
                { "size", Mathf.Clamp(size, 1, 500).ToString() },
                { "direct", HuobiQueryDirectExtensions.GetNickname(direct) },
            };

            if (!currency.IsNullOrEmpty()) parameters.Add("currency", currency);

            if (withdrawid.HasValue) parameters.Add("from", withdrawid.Value.ToString());

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<List<HuobiWalletWithdrawRecord>>();
            return data;
        }

        public async Task<IReadOnlyList<HuobiWalletDepositRecord>> GetDepositRecordsAsync(string currency = null, long? depositid = null, int size = 100, HuobiQueryDirect direct = HuobiQueryDirect.Prev) {
            string route = $@"/v1/query/deposit-withdraw";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "type", "deposit" },
                { "size", Mathf.Clamp(size, 1, 500).ToString() },
                { "direct", HuobiQueryDirectExtensions.GetNickname(direct) },
            };

            if (!currency.IsNullOrEmpty()) parameters.Add("currency", currency);

            if (depositid.HasValue) parameters.Add("from", depositid.Value.ToString());

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<List<HuobiWalletDepositRecord>>();
            return data;
        }

        #endregion 钱包(充值与提现)

        #region 现货/杠杆交易
        /// <summary>
        /// 下市价单
        /// </summary>
        /// <param name="account">账户 ID</param>
        /// <param name="symbol">交易对</param>
        /// <param name="direction">订单方向</param>
        /// <param name="amount">订单交易量</param>
        /// <param name="clientid">用户自编订单号（须在24小时内保持唯一性）</param>
        /// <returns>订单id</returns>
        public async Task<long> MarketOrderAsync(HuobiAccount account, string symbol, HuobiOrderDirection direction, double amount, string clientid = null) {

            account.ThrowIfIsNull(nameof(account));
            symbol.ThrowIfNullOrEmpty(nameof(symbol));

            const string route = @"/v1/order/orders/place";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "account-id", account.ID.ToString() },
                { "symbol", symbol },
                { "type", HuobiOrderTypeExtensions.GetNickname(direction == HuobiOrderDirection.Buy ? HuobiOrderType.市价买入 : HuobiOrderType.市价卖出) },
                { "amount", amount.ToString() },
            };

            if (!clientid.IsNullOrEmpty()) parameters.Add("client-order-id", clientid);

            switch (account.Type)
            {
                case HuobiAccountType.Spot:
                    parameters.Add("source", HuobiOrderSourceExtensions.GetNickname(HuobiOrderSource.现货交易));
                    break;
                case HuobiAccountType.Margin:
                    parameters.Add("source", HuobiOrderSourceExtensions.GetNickname(HuobiOrderSource.杠杆交易));
                    break;
                default:
                    throw new Exception($"LimitOrderAsync()方法不支持当前账号{{ {account.ID}, {account.Type} }}");
            }

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var orderid = response.Data.ToObject<long>();
            return orderid;
        }

        /// <summary>
        /// 下限价单
        /// </summary>
        /// <param name="account">账户 ID</param>
        /// <param name="symbol">交易对</param>
        /// <param name="direction">订单方向</param>
        /// <param name="amount">订单交易量</param>
        /// <param name="price">限价</param>
        /// <param name="clientid">用户自编订单号（须在24小时内保持唯一性）</param>
        /// <returns>订单id</returns>
        public async Task<long> LimitOrderAsync(HuobiAccount account, string symbol, HuobiOrderDirection direction, double amount, double price, string clientid = null) {
            account.ThrowIfIsNull(nameof(account));
            symbol.ThrowIfNullOrEmpty(nameof(symbol));

            const string route = @"/v1/order/orders/place";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "account-id", account.ID.ToString() },
                { "symbol", symbol },
                { "type", HuobiOrderTypeExtensions.GetNickname(direction == HuobiOrderDirection.Buy ? HuobiOrderType.限价买入 : HuobiOrderType.限价卖出) },
                { "amount", amount.ToString() },
                { "price", price.ToString() },
            };

            if (!clientid.IsNullOrEmpty()) parameters.Add("client-order-id", clientid);

            switch (account.Type)
            {
                case HuobiAccountType.Spot:
                    parameters.Add("source", HuobiOrderSourceExtensions.GetNickname(HuobiOrderSource.现货交易));
                    break;
                case HuobiAccountType.Margin:
                    parameters.Add("source", HuobiOrderSourceExtensions.GetNickname(HuobiOrderSource.杠杆交易));
                    break;
                default:
                    throw new Exception($"LimitOrderAsync()方法不支持当前账号{{ {account.ID}, {account.Type} }}");
            }

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var orderid = response.Data.ToObject<long>();
            return orderid;
        }

        /// <summary>
        /// 撤单
        /// </summary>
        /// <returns></returns>
        public async Task<long> CancelOrderAsync(long orderid) {
            string route = $@"/v1/order/orders/{orderid}/submitcancel";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "order-id", orderid.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 根据用户自定义id撤销订单
        /// </summary>
        /// <param name="clientid"></param>
        /// <returns></returns>
        public async Task<long> CancelOrderByClientIdAsync(string clientid) {
            clientid.ThrowIfNullOrEmpty(nameof(clientid));

            const string route = @"/v1/order/orders/submitCancelClientOrder";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "client-order-id", clientid.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 获取未完成订单(不指定账号和交易对)
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiOrder>> GetOpenOrdersAsync(HuobiOrderDirection? direction, int size = 10)
        {
            const string route = @"/v1/order/openOrders";

            size = Mathf.Clamp(size, 1, 500);

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "size", size.ToString() },
            };

            if (direction.HasValue) parameters.Add("side", HuobiOrderDirectionExtensions.GetNickname(direction.Value));

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var orders = response.Data.ToObject<List<HuobiOrder>>();
            return orders;
        }

            /// <summary>
            /// 获取未完成订单(指定账号和交易对)
            /// </summary>
            /// <returns></returns>
            public async Task<IReadOnlyList<HuobiOrder>> GetOpenOrdersAsync(HuobiAccount account, string symbol, HuobiOrderDirection? direction, int size = 10) {
            account.ThrowIfIsNull(nameof(account));
            symbol.ThrowIfNullOrEmpty(nameof(symbol));

            const string route = @"/v1/order/openOrders";
            if (account.Type != HuobiAccountType.Margin && account.Type != HuobiAccountType.Spot) {
                throw new Exception($"GetOpenOrdersAsync()方法不支持当前账号{{ {account.ID}, {account.Type} }}");
            }

            size = Mathf.Clamp(size, 1, 2000);

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "account-id", account.ID.ToString() },
                { "symbol", symbol },
                { "size", size.ToString() },
            };

            if (direction.HasValue) parameters.Add("side", HuobiOrderDirectionExtensions.GetNickname(direction.Value));

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var orders = response.Data.ToObject<List<HuobiOrder>>();
            return orders;
        }

        /// <summary>
        /// 此接口返回指定订单的最新状态和详情。
        /// </summary>
        /// <param name="orderid">订单ID</param>
        /// <returns></returns>
        public async Task<HuobiOrder> GetOrderAsync(long orderid) {
            string route = $@"/v1/order/orders/{orderid}";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "order-id", orderid.ToString() },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var order = response.Data.ToObject<HuobiOrder>();
            return order;
        }

        /// <summary>
        /// 此接口返回指定订单的最新状态和详情。（基于client order ID）
        /// </summary>
        /// <param name="orderid">订单ID</param>
        /// <returns></returns>
        public async Task<HuobiOrder> GetOrderByClientIdAsync(string clientid)
        {
            clientid.ThrowIfNullOrEmpty(nameof(clientid));

            const string route = @"/v1/order/orders/getClientOrder";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "clientOrderId", clientid },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var order = response.Data.ToObject<HuobiOrder>();
            return order;
        }

        /// <summary>
        /// 此接口返回指定订单的成交明细。
        /// </summary>
        /// <param name="orderid">订单ID</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiMatchResult>> GetOrderMatchResultsAsync(long orderid) {
            string route = $@"/v1/order/orders/{orderid}/matchresults";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "order-id", orderid.ToString() },
            };

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var results = response.Data.ToObject<List<HuobiMatchResult>>();
            return results;
        }

        /// <summary>
        /// 当前和历史成交
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="starttime">查询开始日期</param>
        /// <param name="endtime">查询结束日期</param>
        /// <param name="orderid">查询起始ID(最大值)</param>
        /// <param name="direct">查询方向</param>
        /// <param name="size">查询记录大小</param>
        /// <param name="ordertypes">查询的订单类型组合</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiMatchResult>> GetMatchResultsAsync(string symbol, DateTime? starttime, DateTime? endtime, long? orderid, HuobiQueryDirect direct = HuobiQueryDirect.Next, int size = 100, HuobiOrderType[] ordertypes = null)
        {
            symbol.ThrowIfNullOrEmpty(nameof(symbol));

            const string route = @"/v1/order/matchresults";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "direct", HuobiQueryDirectExtensions.GetNickname(direct) },
                { "size", Mathf.Clamp(size, 1, 100).ToString() },
            };

            if (!ordertypes.IsNullOrEmpty())
            {
                var ordertypestrs = ordertypes.Select(type => HuobiOrderTypeExtensions.GetNickname(type));
                parameters.Add("types", string.Join(",", ordertypestrs));
            }
            else {
                parameters.Add("types", "all");
            }

            if(starttime.HasValue) parameters.Add("start-date", starttime.Value.ToString("yyyy-MM-dd"));

            if (endtime.HasValue) parameters.Add("end-date", endtime.Value.ToString("yyyy-MM-dd"));

            if(orderid.HasValue) parameters.Add("from", orderid.Value.ToString());

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var results = response.Data.ToObject<List<HuobiMatchResult>>();
            return results;
        }

        /// <summary>
        /// 搜索历史订单
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="starttime">查询开始日期</param>
        /// <param name="endtime">查询结束日期</param>
        /// <param name="orderid">查询起始ID(最大值)</param>
        /// <param name="direct">查询方向</param>
        /// <param name="size">查询记录大小 [1, 100]</param>
        /// <param name="ordertypes">查询的订单类型组合</param>
        /// <param name="orderstates">查询的订单状态组合</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiOrder>> GetOrdersAsync(string symbol, DateTime? starttime, DateTime? endtime, long? orderid, HuobiQueryDirect? direct, int size = 100, HuobiOrderType[] ordertypes = null, HuobiOrderState[] orderstates = null) {
            symbol.ThrowIfNullOrEmpty(nameof(symbol));

            const string route = @"/v1/order/orders";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
            };

            if (starttime.HasValue) parameters.Add("start-date", starttime.Value.ToString("yyyy-MM-dd"));

            if (endtime.HasValue) parameters.Add("end-date", endtime.Value.ToString("yyyy-MM-dd"));

            if(orderid.HasValue) parameters.Add("from", orderid.Value.ToString());

            if (direct.HasValue) parameters.Add("direct", HuobiQueryDirectExtensions.GetNickname(direct.Value));

            if (!ordertypes.IsNullOrEmpty()) {
                var ordertypestrs = ordertypes.Select(type => HuobiOrderTypeExtensions.GetNickname(type));
                parameters.Add("types", string.Join(",", ordertypestrs));
            };

            if (!orderstates.IsNullOrEmpty()) {
                var orderstatestrs = orderstates.Select(state => HuobiOrderStateExtenstions.GetNickname(state));
                parameters.Add("states", string.Join(",", orderstatestrs));
            }

            parameters.Add("size", Mathf.Clamp(size, 1, 100).ToString());

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var orders = response.Data.ToObject<List<HuobiOrder>>();
            return orders;
        }

        /// <summary>
        /// 搜索最近48小时内历史订单
        /// </summary>
        /// <param name="starttime">查询起始时间（含）</param>
        /// <param name="endtime">查询结束时间（含）</param>
        /// <param name="direct">订单查询方向</param>
        /// <param name="symbol">交易对</param>
        /// <param name="size">每次返回条目数量</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiOrder>> GetOrdersInLast48HoursAsync(DateTime? starttime, DateTime? endtime, HuobiQueryDirect direct = HuobiQueryDirect.Next, string symbol = "all", int size = 100) {
            const string route = @"/v1/order/history";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "size", Mathf.Clamp(size, 10, 1000).ToString() },
                { "direct", HuobiQueryDirectExtensions.GetNickname(direct) },
            };

            if (endtime.HasValue) parameters.Add("end-time", (Mathf.Clamp(endtime.Value, DateTime.Now.AddHours(-48), DateTime.Now) - Global.UnixEpoch).TotalMilliseconds.ToString());

            if (starttime.HasValue) parameters.Add("start-time", (Mathf.Clamp(starttime.Value, DateTime.Now.AddHours(-48), DateTime.Now) - Global.UnixEpoch).TotalMilliseconds.ToString());

            HuobiResponse response = await SendGetRequest(route, HuobiResponse.DataSettings, parameters);

            var orders = response.Data.ToObject<List<HuobiOrder>>();
            return orders;
        }

        /// <summary>
        /// 币币现货账户与合约账户划转, 从现货账户转至合约账户
        /// </summary>
        /// <param name="currency">币种</param>
        /// <param name="amount">划转数量</param>
        /// <returns></returns>
        public async Task<long> ProToFuturesAsync(string currency, double amount) {
            const string route = @"/v1/futures/transfer";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "currency", currency },
                { "amount", amount.ToString() },
                { "type", "pro-to-futures" },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 币币现货账户与合约账户划转, 从合约账户转至现货账户
        /// </summary>
        /// <param name="currency">币种</param>
        /// <param name="amount">划转数量</param>
        /// <returns></returns>
        public async Task<long> FuturesToProAsync(string currency, double amount)
        {
            const string route = @"/v1/futures/transfer";
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "currency", currency },
                { "amount", amount.ToString() },
                { "type", "futures-to-pro" },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }
        #endregion

        #region 借贷
        // 目前杠杆交易仅支持部分以 USDT 和 BTC 为报价币种的交易对。

        /// <summary>
        /// 资产划转, 从现货账户划转至杠杆账户
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="currency"></param>
        /// <param name="amount"></param>
        /// <returns>划转订单id</returns>
        public async Task<long> SpotToMarginAsync(string symbol, string currency, double amount) {
            const string route = @"/v1/dw/transfer-in/margin";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "currency", currency },
                { "amount", amount.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        // 目前杠杆交易仅支持部分以 USDT 和 BTC 为报价币种的交易对。

        /// <summary>
        /// 资产划转, 从杠杆账户划转至现货账户
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="currency"></param>
        /// <param name="amount"></param>
        /// <returns>划转订单id</returns>
        public async Task<long> MarginToSpotAsync(string symbol, string currency, double amount)
        {
            const string route = @"/v1/dw/transfer-out/margin";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "currency", currency },
                { "amount", amount.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 申请借贷
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="currency">币种</param>
        /// <param name="amount">借贷数量</param>
        /// <returns>借贷订单id</returns>
        public async Task<long> MarginOrderAsync(string symbol, string currency, double amount) {
            const string route = @"/v1/margin/orders";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
                { "currency", currency },
                { "amount", amount.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 归还借贷
        /// </summary>
        /// <param name="marginid">借贷订单ID</param>
        /// <param name="amount">归还币种数量</param>
        /// <returns></returns>
        public async Task<long> MarginRepayAsync(long marginid, double amount) {
            string route = $@"/v1/margin/orders/{marginid}/repay";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "amount", amount.ToString() },
            };

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var id = response.Data.ToObject<long>();
            return id;
        }

        /// <summary>
        /// 查询借贷订单
        /// </summary>
        /// <param name="symbol">交易对</param>
        /// <param name="starttime">查询开始日期</param>
        /// <param name="endtime">查询结束日期</param>
        /// <param name="orderid">查询起始 ID</param>
        /// <param name="direct">查询方向</param>
        /// <param name="size">查询记录大小</param>
        /// <param name="states">状态</param>
        /// <returns></returns>
        public async Task<IReadOnlyList<HuobiLoanOrder>> GetMarginLoanOrdersAsync(string symbol, DateTime? starttime, DateTime? endtime, long? orderid, HuobiQueryDirect? direct, int? size, HuobiLoanOrderState[] states = null) {
            const string route = @"/v1/margin/loan-orders";

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "symbol", symbol },
            };

            if (starttime.HasValue) parameters.Add("start-date", starttime.Value.ToString("yyyy-MM-dd"));

            if (endtime.HasValue) parameters.Add("end-date", endtime.Value.ToString("yyyy-MM-dd"));

            if (!states.IsNullOrEmpty()) {
                var statestrs = states.Select(state => HuobiLoanOrderStateExtensions.GetNickname(state));

                parameters.Add("states", string.Join(",", statestrs));
            };

            if (orderid.HasValue) parameters.Add("from", orderid.Value.ToString());

            if (direct.HasValue) parameters.Add("direct", HuobiQueryDirectExtensions.GetNickname(direct.Value));

            if (size.HasValue) parameters.Add("size", size.ToString());

            HuobiResponse response = await SendPostRequest(route, HuobiResponse.DataSettings, parameters);

            var data = response.Data.ToObject<List<HuobiLoanOrder>>();
            return data;
        }

        /// <summary>
        /// 借贷账户详情
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public async Task GetMarginAccountBalancesAsync(string symbol) {
            const string route = @"/v1/margin/accounts/balance";
            throw new NotImplementedException();
        }

        #endregion

        #region ETF (HB10)


        #endregion

        #endregion HuoBiApi方法


        #region HTTP请求方法

        private void ThrowIfError(HuobiResponse response)
        {
            switch (response.Status)
            {
                case HuobiResponseStatus.OK:
                    return;
                case HuobiResponseStatus.Error:
                    var error = response.Data.ToObject<HuobiError>();
                    throw new HuobiException(error.ErrorCode, error.Message);
            }
        }

        private async Task<HuobiResponse> SendGetRequest(string route, JsonSerializerSettings settings, IDictionary<string, string> parameters = null) {
            IDictionary<string, string> dict = GetCommonParameters();

            if (parameters != null) {
                foreach (var kv in parameters)
                {
                    dict.Add(kv.Key, kv.Value);
                }
            }
            dict = UriEncodeParameterValue(dict);

            var signature = GetSignatureStr(HttpMethod.Get, HUOBI_HOST, route, dict);
            dict.Add("Signature", signature);
            
            var parametersStr = string.Join("&", dict.Select(kv => $"{kv.Key}={kv.Value}"));

            var url = $"{HUOBI_HOST_URL}{route}?{parametersStr}";
            var response = await httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            HuobiResponse huobiResponse = JsonConvert.DeserializeObject<HuobiResponse>(result, settings);

            ThrowIfError(huobiResponse);

            return huobiResponse;
        }

        private async Task<HuobiResponse> SendGetRequestWithoutSignature(string route, JsonSerializerSettings settings, IDictionary<string, string> parameters = null) {
            IDictionary<string, string> dict = new Dictionary<string, string>();
            if (parameters != null)
            {
                foreach (var kv in parameters)
                {
                    dict.Add(kv.Key, kv.Value);
                }
            }
            dict = UriEncodeParameterValue(dict);

            var parametersStr = string.Join("&", dict.Select(kv => $"{kv.Key}={kv.Value}"));
            var url = $"{HUOBI_HOST_URL}{route}{(parametersStr.IsNullOrEmpty() ? "" : "?")}{parametersStr}";
            var response = await httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            HuobiResponse huobiResponse = JsonConvert.DeserializeObject<HuobiResponse>(result, settings);

            ThrowIfError(huobiResponse);

            return huobiResponse;
        }

        /// <summary>
        /// 未测试
        /// </summary>
        /// <param name="route"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private async Task<HuobiResponse> SendPostRequest(string route, JsonSerializerSettings settings, IDictionary<string, string> parameters = null) {
            IDictionary<string, string> dict = GetCommonParameters();
            dict = UriEncodeParameterValue(dict);

            var signature = GetSignatureStr(HttpMethod.Get, HUOBI_HOST, route, dict);

            dict.Add("Signature", signature);
            var parametersStr = string.Join("&", dict.Select(kv => $"{kv.Key}={kv.Value}"));

            var url = $"{HUOBI_HOST_URL}{route}?{parametersStr}";
            HttpContent content = new FormUrlEncodedContent(parameters);
            var response = await httpClient.PostAsync(url, content);
            var result = await response.Content.ReadAsStringAsync();
            HuobiResponse huobiResponse = JsonConvert.DeserializeObject<HuobiResponse>(result);
            ThrowIfError(huobiResponse);

            return huobiResponse;
        }

        private IDictionary<string, string> GetCommonParameters() {
            return new Dictionary<string, string>() {
                { "AccessKeyId", apikey.AccessKey },
                { "SignatureMethod", HUOBI_SIGNATURE_METHOD },
                { "SignatureVersion", HUOBI_SIGNATURE_VERSION },
                { "Timestamp", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss") },
                //{ "Timestamp", new DateTime(2019, 7, 16, 11, 34, 0).ToString("yyyy-MM-ddTHH:mm:ss") },
            };
        }

        private IDictionary<string, string> UriEncodeParameterValue(IDictionary<string, string> parameters) {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            foreach (var para in parameters) {
                dict.Add(para.Key, UrlEncode(para.Value));
            }
            return dict;
        }

        private string UrlEncode(string str) {
            StringBuilder builder = new StringBuilder();
            foreach (char c in str)
            {
                if (HttpUtility.UrlEncode(c.ToString(), Encoding.UTF8).Length > 1)
                {
                    builder.Append(HttpUtility.UrlEncode(c.ToString(), Encoding.UTF8).ToUpper());
                }
                else
                {
                    builder.Append(c);
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Hmacsha256加密
        /// </summary>
        /// <param name="message"></param>
        /// <param name="secretKey"></param>
        /// <returns></returns>
        private static string CalculateSignature256(string message, string secretKey) {
            using (var hmacsha256 = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey)))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(message));
                return Convert.ToBase64String(hashmessage);
            }
        }

        /// <summary>
        /// 请求参数签名
        /// </summary>
        /// <param name="method">请求方法</param>
        /// <param name="host">API域名</param>
        /// <param name="route">资源地址</param>
        /// <param name="parameters">请求参数</param>
        /// <returns></returns>
        private string GetSignatureStr(HttpMethod method, string host, string route, IDictionary<string, string> parameters = null) {
            var args = (parameters == null || method == HttpMethod.Post) ? new Dictionary<string, string>() : parameters;

            List<string> list = args.SkipWhile(kv => { return kv.Key.IsNullOrWhiteSpace() || kv.Value.IsNullOrWhiteSpace(); }).Select(kv => { return $"{kv.Key}={kv.Value}"; }).ToList();

            // 参数排序
            list.Sort((s1, s2) => { return string.CompareOrdinal(s1, s2); });

            StringBuilder sb = new StringBuilder();
            sb.Append(method.Method.ToUpper())
                .Append("\n")
                .Append(host)
                .Append("\n")
                .Append(route)
                .Append("\n")
                .Append(string.Join("&", list));

            string message = sb.ToString();
            string sign = CalculateSignature256(message, apikey.SecretKey);
            return UrlEncode(sign);
        }

        #endregion HTTP请求方法
    }
}
