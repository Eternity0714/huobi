﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public enum HuobiStableCoinExchangeState
    {
        /// <summary>
        /// 支持兑入兑出
        /// </summary>
        InOut = 1,
        /// <summary>
        /// 不支持兑入兑出
        /// </summary>
        NotInNotOut = 2,
        /// <summary>
        /// 支持兑入不支持兑出
        /// </summary>
        In = 3,
        /// <summary>
        /// 支持兑出不 支持兑入
        /// </summary>
        Out = 4,
    }
}
