﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiBalanceStateConverter))]
    public enum HuobiBalanceState
    {
        未知 = 0,
        可交易,
        冻结,
        已借贷,
        借贷利息,
        可转出,
        可借贷,
    }

    internal class HuobiBalanceStateConverter : CustomStringEnumConverter<HuobiBalanceState> {
        public HuobiBalanceStateConverter() : base(new Dictionary<HuobiBalanceState, string>() {
            { HuobiBalanceState.可交易, "trade" },
            { HuobiBalanceState.冻结, "frozen" },
            { HuobiBalanceState.已借贷, "loan" },
            { HuobiBalanceState.借贷利息, "interest" },
            { HuobiBalanceState.可转出, "transfer-out-available" },
            { HuobiBalanceState.可借贷, "loan-available" },
        }) { }
    }
}
