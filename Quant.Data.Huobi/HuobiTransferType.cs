﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 资产划转类型
    /// </summary>
    public enum HuobiTransferType
    {
        /// <summary>
        /// 子账号划转给母账户虚拟币
        /// </summary>
        MasterTransferIn,
        /// <summary>
        /// 母账户划转给子账号虚拟币
        /// </summary>
        MasterTransferOut,
        /// <summary>
        /// 子账号划转给母账户点卡
        /// </summary>
        MasterPointTransferIn,
        /// <summary>
        /// 母账户划转给子账号点卡
        /// </summary>
        MasterPointTransferOut,
    }

    internal static class HuobiTransferTypeExtensions
    {
        public static readonly IReadOnlyDictionary<HuobiTransferType, string> Nicknames = new Dictionary<HuobiTransferType, string>() {
            { HuobiTransferType.MasterTransferIn, "master-transfer-in" },
            { HuobiTransferType.MasterTransferOut, "master-transfer-out" },
            { HuobiTransferType.MasterPointTransferIn, "master-point-transfer-in" },
            { HuobiTransferType.MasterPointTransferOut, "master-point-transfer-out" },
        };

        public static string GetNickname(this HuobiTransferType step)
        {
            return Nicknames[step];
        }
    }


    internal class HuobiTransferTypeConverter : CustomStringEnumConverter<HuobiTransferType>
    {
        public HuobiTransferTypeConverter() : base(HuobiTransferTypeExtensions.Nicknames) { }
    }
}
