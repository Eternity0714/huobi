﻿using Newtonsoft.Json;
using Quant.Core;
using System;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 借贷订单
    /// </summary>
    public class HuobiLoanOrder
    {
        /// <summary>
        /// 订单号
        /// </summary>
        [JsonProperty("id")]
        public long ID { get; internal set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        [JsonProperty("user-id")]
        public long UserID { get; internal set; }

        /// <summary>
        /// 账户ID
        /// </summary>
        [JsonProperty("account-id")]
        public long AccountID { get; internal set; }

        /// <summary>
        /// 交易对
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; internal set; }

        /// <summary>
        /// 币种
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; internal set; }

        /// <summary>
        /// 借贷本金总额
        /// </summary>
        [JsonProperty("loan-amount")]
        public double LoanAmount { get; internal set; }

        /// <summary>
        /// 未还本金
        /// </summary>
        [JsonProperty("loan-balance")]
        public double LoanBalance { get; internal set; }

        /// <summary>
        /// 利率
        /// </summary>
        [JsonProperty("interest-rate")]
        public double InterestRate { get; internal set; }

        /// <summary>
        /// 利息总额
        /// </summary>
        [JsonProperty("interest-amount")]
        public double InterestAmount { get; internal set; }

        /// <summary>
        /// 未还利息
        /// </summary>
        [JsonProperty("interest-balance")]
        public double InterestBalance { get; internal set; }

        [JsonProperty("created-at")]
        private long _createtime;

        /// <summary>
        /// 借贷发起时间
        /// </summary>
        [JsonIgnore]
        public DateTime CreateTime {
            get {
                return Global.UnixEpoch.AddMilliseconds(_createtime);
            }
        }

        /// <summary>
        /// 最近一次计息时间
        /// </summary>
        [JsonProperty("accrued-at")]
        private long _accruedtime;

        [JsonIgnore]
        public DateTime AccruedTime {
            get {
                return Global.UnixEpoch.AddMilliseconds(_accruedtime);
            }
        }

        /// <summary>
        /// 订单状态
        /// </summary>
        [JsonProperty("state")]
        public HuobiLoanOrderState LoanState { get; internal set; }
    }
}
