﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiBalance
    {
        [JsonProperty("balance")]
        public float Balance { get; internal set; }

        [JsonProperty("currency")]
        public float Currency { get; internal set; }

        [JsonProperty("type")]
        public HuobiBalanceState State { get; internal set; } = HuobiBalanceState.未知;
    }
}
