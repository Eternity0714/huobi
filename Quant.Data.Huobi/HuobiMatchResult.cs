﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Quant.Core;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 撮合结果
    /// </summary>
    public class HuobiMatchResult
    {
        /// <summary>
        /// 订单成交记录ID
        /// </summary>
        [JsonProperty("id")]
        public long ID { get; internal set; }

        /// <summary>
        /// 订单ID
        /// </summary>
        [JsonProperty("order-id")]
        public long OrderID { get; internal set; }

        /// <summary>
        /// 撮合ID
        /// </summary>
        [JsonProperty("match-id")]
        public long MatchID { get; internal set; }

        /// <summary>
        /// 交易对
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; internal set; }

        /// <summary>
        /// 订单类型
        /// </summary>
        [JsonProperty("type")]
        public HuobiOrderType OrderType { get; internal set; }

        /// <summary>
        /// 订单来源
        /// </summary>
        [JsonProperty("source")]
        public HuobiOrderSource OrderSource { get; internal set; }

        /// <summary>
        /// 成交价格
        /// </summary>
        [JsonProperty("price")]
        public double Price { get; internal set; }

        /// <summary>
        /// 成交数量
        /// </summary>
        [JsonProperty("filled-amount")]
        public double FilledAmount { get; internal set; }

        /// <summary>
        /// 成交手续费（买入为基础币，卖出为计价币）
        /// </summary>
        [JsonProperty("filled-fees")]
        public double FilledFees { get; internal set; }

        [JsonProperty("created-at")]
        private long _createtime;

        /// <summary>
        /// 成交时间
        /// </summary>
        [JsonIgnore]
        public DateTime CreateTime {
            get {
                return Global.UnixEpoch.AddMilliseconds(_createtime).ToLocalTime();
            }
        }
    }
}
