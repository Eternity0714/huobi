﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiSymbolPartitionConverter))]
    public enum HuobiSymbolPartition
    {
        /// <summary>
        /// 主区
        /// </summary>
        Main = 1,
        /// <summary>
        /// 创新区
        /// </summary>
        Innovation = 2,
        /// <summary>
        /// 分叉区
        /// </summary>
        Bifurcation = 3,
    }

    public class HuobiSymbolPartitionConverter : CustomStringEnumConverter<HuobiSymbolPartition>
    {
        public HuobiSymbolPartitionConverter() : base(new Dictionary<HuobiSymbolPartition, string>() {
            { HuobiSymbolPartition.Main, "main" },
            { HuobiSymbolPartition.Innovation, "innovation" },
            { HuobiSymbolPartition.Bifurcation, "bifurcation" },
        })
        { }
    }
}
