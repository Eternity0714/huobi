﻿using Newtonsoft.Json.Linq;
using Quant.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Quant.Data.Huobi
{
    public class BusinessWsProvider {
        private WebSocketSession ws;

        public BusinessWsProvider() {
            ws = new WebSocketSession("wss://api.huobi.pro/ws/v1");
            ws.OnReceive = OnReceive;
        }

        private async Task OnReceive(string msg)
        {
            Console.WriteLine($"已接收市场数据: {msg}");
            JObject response = JObject.Parse(msg);
            if (response.ContainsKey("ping"))
            {
                JObject request = new JObject();
                request["pong"] = response.Value<long>("ping");
                await ws.SendAsync(request.ToString());
            }
            if (response.ContainsKey("op"))
            {
                var op = response["op"].ToObject<string>();
                if (op == "sub")
                {

                }
                else if (op == "unsub")
                {

                }
                else if (op == "notify")
                {

                }
                else if (op == "auth")
                {

                }
                else
                {
                    Console.WriteLine($"未处理消息: {msg}");
                }
            }
            else
            {
                Console.WriteLine($"未处理消息: {msg}");
            }
        }
    }

    /// <summary>
    /// 设计要求: 仅监听消息，不主动请求数据
    /// </summary>
    public class MarketWsProvider
    {
        private WebSocketSession ws;
        private ulong clientid = 0;

        private IDictionary<string, IList<IActionObject>> handlerMaps = new ConcurrentDictionary<string, IList<IActionObject>>();

        public MarketWsProvider()
        {
            ws = new WebSocketSession("wss://api.huobi.pro/ws");
            ws.OnReceive = OnReceive;
        }

        private async Task SendAsync(string msg) {
            await ws.SendAsync(msg);
        }

        private async Task OnReceive(string msg)
        {
            JObject response = JObject.Parse(msg);
            if (response.ContainsKey("ping"))
            {
                JObject request = new JObject();
                request["pong"] = response.Value<long>("ping");
                await SendAsync(request.ToString());
                return;
            }

            if (response.ContainsKey("subbed")) return;

            if (response.ContainsKey("unsubbed")) return;

            if (response.ContainsKey("ch"))
            {
                Console.WriteLine(msg);
                string ch = response["ch"].ToObject<string>();
                if (handlerMaps.TryGetValue(ch, out var handlers)) {
                    string[] chs = ch.Split('.');
                    if (chs[2] == "kline")
                    {
                        var kline = response["tick"].ToObject<HuobiCandleChart>();
                        foreach (var handler in handlers) {
                            handler.Invoke(kline);
                        }
                        return;
                    }
                }
            }

            Console.WriteLine($"未处理消息: {msg}");
        }

        private async Task Subscribe<T>(string key, Action<T> action) where T : class
        {
            if (action == null) return;
            if (!handlerMaps.TryGetValue(key, out var handlers))
            {
                JObject request = new JObject();
                request["sub"] = key;
                request["id"] = clientid++.ToString();
                await SendAsync(request.ToString());

                handlers = new List<IActionObject>();
                handlerMaps.Add(key, handlers);
            }

            if (!handlers.Any(item => item.Equals(action)))
            {
                handlers.Add(new ActionObject<T>(action));
            }

        }

        private async Task Unsubscribe<T>(string key, Action<T> action) where T : class
        {
            if (action == null) return;
            if (!handlerMaps.TryGetValue(key, out var handlers)) return;
            handlers = handlers.Where(item => !item.Equals(action)).ToList();

            if (handlers.Count == 0)
            {
                handlerMaps.Remove(key);

                JObject request = new JObject();
                request["unsub"] = key;
                request["id"] = clientid++.ToString();
                await SendAsync(request.ToString());
            }
            else
            {
                handlerMaps[key] = handlers;
            }
        }

        public async Task AddKlineListener(string symbol, HuobiPeriod period, Action<HuobiCandleChart> action)
        {
            string key = $"market.{symbol}.kline.{HuobiPeriodHelper.GetDescription(period)}";
            await Subscribe(key, action);
        }

        public async Task RemoveKlineListener<T>(string symbol, HuobiPeriod period, Action<HuobiCandleChart> action)
        {
            string key = $"market.{symbol}.kline.{HuobiPeriodHelper.GetDescription(period)}";
            await Unsubscribe(key, action);
        }
    }

    public class WebSocketSession 
        //: IDisposable
    {
        private ClientWebSocket websocket = new ClientWebSocket();
        private CancellationTokenSource receiveTokenSource = new CancellationTokenSource();

        private Task task;

        public Func<string, Task> OnReceive;

        public WebSocketSession(string url) {
            Console.WriteLine("正在连接...");
            websocket.ConnectAsync(new Uri(url), CancellationToken.None).Wait();
            Console.WriteLine("连接成功");
            task = Task.Factory.StartNew(async () => {
                while (true) {
                    await ReceiveAsync();
                }
            }, receiveTokenSource.Token);
        }

        public async Task SendAsync(string msg) {
            await websocket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(msg)), WebSocketMessageType.Text, true, CancellationToken.None);
        }

        private async Task ReceiveAsync()
        {
            byte[] bytes = new byte[1024];

            using (MemoryStream ms = new MemoryStream())
            {
                WebSocketReceiveResult result = null;
                do
                {
                    result = await websocket.ReceiveAsync(new ArraySegment<byte>(bytes), CancellationToken.None);
                    await ms.WriteAsync(bytes, 0, result.Count);
                } while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);
                using (MemoryStream output = new MemoryStream())
                {
                    using (GZipStream gzip = new GZipStream(ms, CompressionMode.Decompress))
                    {
                        int size = 1024;
                        byte[] temp = new byte[size];
                        int count = 0;
                        while ((count = await gzip.ReadAsync(temp, 0, size)) > 0)
                        {
                            await output.WriteAsync(temp, 0, count);
                        }
                    }
                    byte[] data = output.ToArray();
                    string msg = Encoding.UTF8.GetString(data);
                    //
                    try
                    {
                        await OnReceive?.Invoke(msg);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                    
                }
            }
        }
    }

    internal interface IActionObject {
        void Invoke(object data);

        bool Equals(Action<object> func);
    }

    internal interface IActionObject<T> : IActionObject
    {
        void Invoke(T data);

        bool Equals(Action<T> func);
    }

    internal class ActionObject<T> : IActionObject<T> where T : class {
        private Action<T> func;

        public ActionObject(Action<T> func)
        {
            this.func = func ?? throw new ArgumentNullException(nameof(func));
        }

        public void Invoke(T data)
        {
            func.Invoke(data);
        }

        public bool Equals(Action<T> action)
        {
            return GetHashCode() == action.GetHashCode();
        }

        public override int GetHashCode()
        {
            return func.GetHashCode();
        }

        void IActionObject.Invoke(object data)
        {
            Invoke(data as T);
        }

        bool IActionObject.Equals(Action<object> action)
        {
            return GetHashCode() == action.GetHashCode();
        }
    }
}
