﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data.Huobi
{
    public class HuobiAccount
    {
        internal HuobiAccount() { }

        [JsonProperty("id")]
        public long ID { get; internal set; }

        [JsonProperty("type")]
        public HuobiAccountType Type { get; internal set; }

        [JsonProperty("state")]
        public HuobiAccountState State { get; internal set; }

        [JsonProperty("subtype")]
        public string SubType { get; internal set; }
    }
}
