﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 交易对状态
    /// </summary>
    [JsonConverter(typeof(HuobiSymbolStateConverter))]
    public enum HuobiSymbolState
    {
        /// <summary>
        /// 已上线
        /// </summary>
        Online = 1,
        /// <summary>
        /// 交易对已下线，不可交易
        /// </summary>
        Offline = 2,
        /// <summary>
        /// 交易暂停
        /// </summary>
        Suspend = 3,
    }

    public class HuobiSymbolStateConverter : CustomStringEnumConverter<HuobiSymbolState>
    {
        public HuobiSymbolStateConverter() : base(new Dictionary<HuobiSymbolState, string>() {
            { HuobiSymbolState.Online, "online" },
            { HuobiSymbolState.Offline, "offline" },
            { HuobiSymbolState.Suspend, "suspend" },
        })
        { }
    }
}
