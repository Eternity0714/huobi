﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 账户类型
    /// </summary>
    [JsonConverter(typeof(HuobiAccountTypeConverter))]
    public enum HuobiAccountType
    {
        /// <summary>
        /// 现货账户
        /// </summary>
        Spot,
        /// <summary>
        /// 杠杆账户
        /// </summary>
        Margin,
        /// <summary>
        /// OTC 账户
        /// </summary>
        Otc,
        /// <summary>
        /// 点卡账户
        /// </summary>
        Point,
    }

    internal class HuobiAccountTypeConverter : CustomStringEnumConverter<HuobiAccountType>
    {
        public HuobiAccountTypeConverter() : base(new Dictionary<HuobiAccountType, string>() {
            { HuobiAccountType.Spot, "spot" },
            { HuobiAccountType.Margin, "margin" },
            { HuobiAccountType.Otc, "otc" },
            { HuobiAccountType.Point, "point" },
        })
        { }
    }
}
