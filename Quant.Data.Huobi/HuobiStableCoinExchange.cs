﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Quant.Core;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 稳定币兑换记录
    /// </summary>
    public class HuobiStableCoinExchange
    {
        /// <summary>
        /// 兑换记录 ID
        /// </summary>
        [JsonProperty("id")]
        public long ID { get; internal set; }

        /// <summary>
        /// 稳定币币种
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; internal set; }

        /// <summary>
        /// 兑换方向, buy兑入/sell兑出
        /// </summary>
        [JsonProperty("type")]
        public HuobiOrderDirection Direction { get; internal set; }

        /// <summary>
        /// 兑入或兑出的稳定币金额数量
        /// </summary>
        [JsonProperty("amount")]
        public double Amount { get; internal set; }

        /// <summary>
        /// 兑换费率，即此次兑换操作的汇率
        /// </summary>
        [JsonProperty("rate")]
        public double Rate { get; internal set; }


        /// <summary>
        /// 成功兑入或兑出的 HUSD 数量
        /// </summary>
        [JsonProperty("exchange_amount")]
        public double ExchangeAmount { get; internal set; }

        [JsonProperty("time")]
        private long _ts;

        /// <summary>
        /// 时间戳
        /// </summary>
        [JsonIgnore]
        public DateTime Time
        {
            get
            {
                return Global.UnixEpoch.AddMilliseconds(_ts).ToLocalTime();
            }
        }
    }
}
