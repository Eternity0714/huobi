﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Quant.Core;

namespace Quant.Data.Huobi
{
    public class HuobiMergedTicker : HuobiCandleChart
    {
        public HuobiMergedTicker() : base() { }

        [JsonProperty("ask")]
        public HuobiDelegate Ask { get; internal set; }

        [JsonProperty("bid")]
        public HuobiDelegate Bid { get; internal set; }
    }
}
