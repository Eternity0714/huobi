﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiOrderTypeConverter))]
    public enum HuobiOrderType
    {
        市价买入 = 1,
        市价卖出 = 2,
        限价买入 = 3,
        限价卖出 = 4,
        IOC买入 = 5,
        IOC卖出 = 6,
    }

    internal static class HuobiOrderTypeExtensions {
        public static readonly IReadOnlyDictionary<HuobiOrderType, string> Nicknames = new Dictionary<HuobiOrderType, string>() {
            { HuobiOrderType.市价买入, "buy-market" },
            { HuobiOrderType.市价卖出, "sell-market" },
            { HuobiOrderType.限价买入, "buy-limit" },
            { HuobiOrderType.限价卖出, "sell-limit" },
            { HuobiOrderType.IOC买入, "buy-ioc" },
            { HuobiOrderType.IOC卖出, "sell-ioc" },
        };

        public static string GetNickname(this HuobiOrderType type)
        {
            return Nicknames[type];
        }
    }

    internal class HuobiOrderTypeConverter : CustomStringEnumConverter<HuobiOrderType>
    {
        public HuobiOrderTypeConverter() : base(HuobiOrderTypeExtensions.Nicknames)
        { }
    }
}
