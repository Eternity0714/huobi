﻿using Newtonsoft.Json;
using Quant.Core;
using System;

namespace Quant.Data.Huobi
{
    public class HuobiTrade
    {
        internal HuobiTrade() { }

        public long ID { get; internal set; }

        public double Amount { get; internal set; }

        public double Price { get; internal set; }

        public HuobiOrderDirection Direction { get; internal set; }

        private long ts;

        [JsonIgnore]
        public DateTime Timestemp {
            get {
                return Global.UnixEpoch.AddMilliseconds(ts);
            }
        }
    }
}
