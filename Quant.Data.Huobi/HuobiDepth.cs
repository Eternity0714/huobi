﻿using Newtonsoft.Json;
using Quant.Core;
using System;

namespace Quant.Data.Huobi
{
    public class HuobiDepth
    {
        internal HuobiDepth() { }

        private long ts;

        [JsonIgnore]
        public DateTime Timestemp {
            get {
                return Global.UnixEpoch.AddMilliseconds(ts).ToLocalTime();
            }
        }

        //[JsonProperty("bids")]
        public HuobiDelegate[] Bids { get; internal set; }

        //[JsonProperty("asks")]
        public HuobiDelegate[] Asks { get; internal set; }
    }
}
