﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiLoanOrderStateConverter))]
    public enum HuobiLoanOrderState
    {
        未放款,
        已放款,
        已还清,
        异常,
    }

    internal static class HuobiLoanOrderStateExtensions {
        public static readonly IReadOnlyDictionary<HuobiLoanOrderState, string> Nicknames = new Dictionary<HuobiLoanOrderState, string>() {
            { HuobiLoanOrderState.未放款, "created" },
            { HuobiLoanOrderState.已放款, "accrual" },
            { HuobiLoanOrderState.已还清, "cleared" },
            { HuobiLoanOrderState.异常, "invalid " },
        };

        public static string GetNickname(this HuobiLoanOrderState state)
        {
            return Nicknames[state];
        }
    }

    internal class HuobiLoanOrderStateConverter : CustomStringEnumConverter<HuobiLoanOrderState>
    {
        public HuobiLoanOrderStateConverter() : base(HuobiLoanOrderStateExtensions.Nicknames)
        { }
    }
}
