﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Quant.Core;

namespace Quant.Data.Huobi
{
    internal class HuobiResponse
    {
        public static readonly JsonSerializerSettings DataSettings = new JsonSerializerSettings() {
            ContractResolver = new PropsContractResolver(new Dictionary<string, string>() {
                { "Data", "data" },
            }),
        };

        public static readonly JsonSerializerSettings TickSettings = new JsonSerializerSettings()
        {
            ContractResolver = new PropsContractResolver(new Dictionary<string, string>() {
                { "Data", "tick" },
            }),
        };


        [JsonProperty("status")]
        public HuobiResponseStatus Status { get; private set; }


        [JsonProperty("ts")]
        private long _ts;

        [JsonIgnore]
        public DateTime Timestemp {
            get {
                return Global.UnixEpoch.AddMilliseconds(_ts).ToLocalTime();
            }
        }

        [JsonProperty]
        public JToken Data { get; private set; }
    }
}
