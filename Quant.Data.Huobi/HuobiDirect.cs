﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiQueryDirectConverter))]
    public enum HuobiQueryDirect
    {
        Prev = 0,
        Next = 1,
    }

    internal static class HuobiQueryDirectExtensions {
        public static readonly IReadOnlyDictionary<HuobiQueryDirect, string> Nicknames = new Dictionary<HuobiQueryDirect, string>() {
            { HuobiQueryDirect.Prev, "prev" },
            { HuobiQueryDirect.Next, "next" },
        };

        public static string GetNickname(this HuobiQueryDirect direct) {
            return Nicknames[direct];
        }
    }

    public class HuobiQueryDirectConverter : CustomStringEnumConverter<HuobiQueryDirect>
    {
        public HuobiQueryDirectConverter() : base(HuobiQueryDirectExtensions.Nicknames)
        { }
    }
}
