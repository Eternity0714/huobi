﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    [JsonConverter(typeof(HuobiOrderSourceConverter))]
    public enum HuobiOrderSource
    {
        现货交易 = 1,
        杠杆交易 = 2,
    }

    internal static class HuobiOrderSourceExtensions
    {
        public static readonly IReadOnlyDictionary<HuobiOrderSource, string> Nicknames = new Dictionary<HuobiOrderSource, string>() {
            { HuobiOrderSource.现货交易, "api" },
            { HuobiOrderSource.杠杆交易, "margin-api" },
        };

        public static string GetNickname(this HuobiOrderSource source)
        {
            return Nicknames[source];
        }
    }

    internal class HuobiOrderSourceConverter : CustomStringEnumConverter<HuobiOrderSource>
    {
        public HuobiOrderSourceConverter() : base(HuobiOrderSourceExtensions.Nicknames)
        { }
    }
}
