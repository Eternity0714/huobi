﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 账户状态
    /// </summary>
    [JsonConverter(typeof(HuobiAccountStateConverter))]
    public enum HuobiAccountState
    {

        /// <summary>
        /// 正常
        /// </summary>
        Working,
        /// <summary>
        /// 账户被锁定
        /// </summary>
        Lock,
    }

    internal class HuobiAccountStateConverter : CustomStringEnumConverter<HuobiAccountState> {
        public HuobiAccountStateConverter() : base(new Dictionary<HuobiAccountState, string>() {
            { HuobiAccountState.Working, "working" },
            { HuobiAccountState.Lock, "lock" },
        }) { }
    }
}
