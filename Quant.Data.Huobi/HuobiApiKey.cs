﻿using System;

namespace Quant.Data.Huobi
{
    public sealed class HuobiApiKey
    {
        /// <summary>
        /// API 访问密钥
        /// </summary>
        public string AccessKey { get; private set; }

        /// <summary>
        /// 签名认证加密所使用的密钥（仅申请时可见）
        /// </summary>
        public string SecretKey { get; private set; }

        public HuobiApiKey(string accessKey, string secretKey)
        {
            if (string.IsNullOrEmpty(accessKey)) throw new ArgumentNullException(nameof(accessKey));
            if (string.IsNullOrEmpty(secretKey)) throw new ArgumentNullException(nameof(secretKey));
            AccessKey = accessKey;
            SecretKey = secretKey;
        }
    }
}
