﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Quant.Core;

namespace Quant.Data.Huobi
{
    /// <summary>
    /// 订单数据
    /// </summary>
    public class HuobiOrder
    {
        /// <summary>
        /// 订单id
        /// </summary>
        [JsonProperty("id")]
        public long ID { get; internal set; }

        /// <summary>
        /// 交易对
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; internal set; }

        /// <summary>
        /// 交易账号
        /// </summary>
        [JsonProperty("account-id")]
        public long AccountID { get; internal set; }

        /// <summary>
        /// 委托价
        /// </summary>
        [JsonProperty("price")]
        public double Price { get; internal set; }

        /// <summary>
        /// 委托量
        /// </summary>
        [JsonProperty("amount")]
        public double Amount { get; internal set; }

        [JsonProperty("created-at")]
        private long _createtime;

        /// <summary>
        /// 订单创建时间
        /// </summary>
        [JsonIgnore]
        public DateTime CreateTime {
            get {
                return Global.UnixEpoch.AddMilliseconds(_createtime).ToLocalTime();
            }
        }

        [JsonProperty("canceled-at")]
        private long _canceltime;

        /// <summary>
        /// 订单取消时间
        /// </summary>
        [JsonIgnore]
        public DateTime CancelTime
        {
            get
            {
                return Global.UnixEpoch.AddMilliseconds(_canceltime).ToLocalTime();
            }
        }

        [JsonProperty("finished-at")]
        private long _finishedtime;

        /// <summary>
        /// 订单结束时间
        /// </summary>
        [JsonIgnore]
        public DateTime FinishedTime
        {
            get
            {
                return Global.UnixEpoch.AddMilliseconds(_finishedtime).ToLocalTime();
            }
        }

        /// <summary>
        /// 订单类型
        /// </summary>
        [JsonProperty("type")]
        public HuobiOrderType OrderType { get; internal set; }

        /// <summary>
        /// 订单中已成交部分的数量
        /// </summary>
        [JsonProperty("filled-amount")]
        public double FilledAmount { get; internal set; }

        /// <summary>
        /// 订单中已成交部分的总金额
        /// </summary>
        [JsonProperty("filled-cash-amount")]
        public double FilledCash { get; internal set; }

        /// <summary>
        /// 已交交易手续费总额（买入为基础币，卖出为计价币）
        /// </summary>
        [JsonProperty("filled-fees")]
        public double FilledFees { get; internal set; }

        [JsonProperty("source")]
        public HuobiOrderSource OrderSource { get; internal set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        [JsonProperty("state")]
        public HuobiOrderState OrderState { get; internal set; }
    }
}
