﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    public class RawStochasticValueCollection : ExtMarketDataCollection<float?>
    {
        private KlineCollection klines;

        public int Period { get; private set; }

        private RawStochasticValueCollection(KlineCollection klines, int period)
        {
            this.klines = klines;
            this.Period = period;
        }

        public override void OnInit()
        {
            if (klines.Count > 0) {
                base.OnAddOrUpdate(klines.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            int offset = Period - 1;
            if (index < offset) return null;

            int skip = index - offset;
            float close = klines[index].Close;
            float high = klines.Skip(skip).Take(Period).Max(item => item.High);
            float low = klines.Skip(skip).Take(Period).Min(item => item.Low);
            return(close - low) / (high - low) * 100f;
        }

        public static RawStochasticValueCollection Get(MarketDataCollectionFactory factory, int period)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new RawStochasticValueCollection(factory.Klines, period);
            });
        }
    }
}
