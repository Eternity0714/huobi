﻿using Quant.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quant.Collection
{
    public class MarketDataCollectionFactory
    {
        public KlineCollection Klines { get; protected set; }

        public int KlineDataCount { get { return Klines.Count; } }

        private IDictionary<Type, IList<IEntity>> dict = new Dictionary<Type, IList<IEntity>>();

        private IList<IEntity> entities = new List<IEntity>();

        public Action<int> OnDataAddOrUpdate { get; private set; }

        public MarketDataCollectionFactory() {
            this.Klines = new KlineCollection();
        }

        public void AddOrUpdateKlineData(CandleChart data) {
            int index = Klines.Count == 0 || Klines[Klines.Count - 1].OpenTime != data.OpenTime ? Klines.Add(data) : Klines.Update(data);
            AfterKlineDataAddOrUpdate(index);
        }

        private void AfterKlineDataAddOrUpdate(int index) {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].GetCollection().OnAddOrUpdate(index);
            }
            OnDataAddOrUpdate?.Invoke(index);
        }

        internal T GetCollection<T>(Dictionary<string, object> parameters, Func<T> newEntityFunc) where T : class, IExtMarketDataCollection
        {
            parameters = parameters ?? new Dictionary<string, object>();
            Type type = typeof(T);
            if (!dict.TryGetValue(type, out var list))
            {
                list = new List<IEntity>(1);
                dict.Add(type, list);
            }
            IEntity entity = list.FirstOrDefault(item => item.Compare(parameters));
            if (entity == null)
            {
                var collection = newEntityFunc();
                collection.OnInit();
                entity = new Entity<T>(parameters, collection);
            }

            return (entity as Entity<T>).GetCollection();
        }

        private interface IEntity
        {
            bool Compare(Dictionary<string, object> parameters);

            IExtMarketDataCollection GetCollection();
        }

        private class Entity<T> : IEntity where T : class, IExtMarketDataCollection
        {
            private Dictionary<string, object> Parameters;

            private T Collection;

            public Entity(Dictionary<string, object> parameters, T collection)
            {
                this.Parameters = parameters;
                this.Collection = collection;
            }

            public bool Compare(Dictionary<string, object> parameters)
            {
                if (Parameters.Count != parameters.Count) return false;
                if (Parameters.Any(kv => !parameters.ContainsKey(kv.Key) || parameters[kv.Key] != kv.Value)) return false;
                return true;
            }

            bool IEntity.Compare(Dictionary<string, object> parameters)
            {
                return Compare(parameters);
            }

            public T GetCollection()
            {
                return Collection;
            }

            IExtMarketDataCollection IEntity.GetCollection()
            {
                return GetCollection();
            }
        }
    }
}
