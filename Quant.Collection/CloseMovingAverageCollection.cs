﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    public class CloseMovingAverageCollection : ExtMarketDataCollection<float?>
    {
        public int Period { get; private set; }

        private KlineCollection klines;

        private CloseMovingAverageCollection(KlineCollection klines, int period) {
            this.Period = period;
            this.klines = klines;
        }

        public override void OnInit()
        {
            if (klines.Count > 0)
            {
                base.OnAddOrUpdate(klines.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            int offset = Period - 1;
            if (index < offset) return null;

            var value = klines.Skip(index - offset).Take(Period).Average(item => item.Close);
            return value;
        }

        public static CloseMovingAverageCollection Get(MarketDataCollectionFactory factory, int period)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new CloseMovingAverageCollection(factory.Klines, period);
            });
        }
    }
}
