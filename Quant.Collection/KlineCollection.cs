﻿using Quant.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public sealed class KlineCollection : MarketDataCollection<CandleChart>
    {
        //public Action<int> OnDataAddOrUpdate { get; private set; }

        public KlineCollection() : base() { }

        internal int Add(CandleChart data)
        {
            int index = datas.Count;
            datas.Add(data);
            return index;
        }

        internal int Update(CandleChart data)
        {
            int index = datas.Count - 1;
            datas[index] = data;
            return index;
        }
    }
}
