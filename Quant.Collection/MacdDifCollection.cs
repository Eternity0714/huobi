﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public class MacdDifCollection : ExtMarketDataCollection<float?>
    {
        private CloseExponentialMovingAverageCollection fast;

        private CloseExponentialMovingAverageCollection slow;

        public int DifPeriod { get; private set; }

        public int FastPeriod => fast.Period;

        public int SlowPeriod => slow.Period;

        private MacdDifCollection(CloseExponentialMovingAverageCollection fast, CloseExponentialMovingAverageCollection slow, int difPeriod)
        {
            this.fast = fast;
            this.slow = slow;
            this.DifPeriod = difPeriod;
        }

        public override void OnInit()
        {
            if (fast.Count > 0)
            {
                base.OnAddOrUpdate(fast.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            int offset = SlowPeriod - 1;
            if (index < offset) return null;
            return fast[index].Value - slow[index].Value;
        }

        public static MacdDifCollection Get(MarketDataCollectionFactory factory, int difPeriod = 9, int fastPeriod = 12, int slowPeriod = 26)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "difPeriod", difPeriod },
                    { "fastPeriod", fastPeriod },
                    { "slowPeriod", slowPeriod },
                }, 
                () => {
                return new MacdDifCollection(CloseExponentialMovingAverageCollection.Get(factory, fastPeriod), CloseExponentialMovingAverageCollection.Get(factory, slowPeriod), difPeriod);
            });
        }
    }
}
