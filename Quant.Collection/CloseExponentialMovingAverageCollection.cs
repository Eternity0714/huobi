﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    public class CloseExponentialMovingAverageCollection : ExtMarketDataCollection<float?>
    {
        public int Period { get; private set; }

        private KlineCollection klines;

        private CloseExponentialMovingAverageCollection(KlineCollection klines, int period)
        {
            this.Period = period;
            this.klines = klines;
        }

        public override void OnInit()
        {
            if (klines.Count > 0)
            {
                base.OnAddOrUpdate(klines.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            float alpha = 2f / (1 + Period);
            int offset = Period - 1;
            if (index < offset) return null;
            if (index == offset) return klines.Take(Period).Average(item => item.Close);

            var ema = datas[index - 1].Value;
            return (klines[index].Close - ema) * alpha + ema;
        }

        public static CloseExponentialMovingAverageCollection Get(MarketDataCollectionFactory factory, int period)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new CloseExponentialMovingAverageCollection(factory.Klines, period);
            });
        }
    }
}
