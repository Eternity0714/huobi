﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public class CloseAbstractUpCollection : ExtMarketDataCollection<float?>
    {
        private CloseUpDownCollection values;

        private CloseAbstractUpCollection(CloseUpDownCollection values)
        {
            this.values = values;
        }

        public override void OnInit()
        {
            if (values.Count > 0)
            {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        protected override float? Calculate(int index) 
        {
            var value = values[index];
            return value.HasValue ? Math.Max(value.Value, 0) : value;
        }

        public static CloseAbstractUpCollection Get(MarketDataCollectionFactory factory)
        {
            return factory.GetCollection(null, () => {
                return new CloseAbstractUpCollection(CloseUpDownCollection.Get(factory));
            });
        }
    }
}
