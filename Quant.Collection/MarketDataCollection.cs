﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public abstract class MarketDataCollection<T> : IEnumerable<T>, IEnumerable, IReadOnlyList<T>
    {
        protected IList<T> datas;

        public MarketDataCollection()
        {
            datas = new List<T>();
        }

        public T this[int index] => GetValue(index);

        public virtual int Count => datas.Count;

        public virtual IEnumerator<T> GetEnumerator()
        {
            return datas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual T GetValue(int index)
        {
            return datas[index];
        }
    }
}
