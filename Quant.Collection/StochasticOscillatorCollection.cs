﻿using Quant.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    /// <summary>
    /// kdj指标
    /// </summary>
    public class StochasticOscillatorCollection : ExtMarketDataCollection<StochasticOscillator>
    {
        private RawStochasticValueCollection values;

        public int Period => values.Period;

        public int M1 { get; private set; }

        public int M2 { get; private set; }

        private StochasticOscillatorCollection(RawStochasticValueCollection rsv,  int ma1 = 1, int ma2 = 3)
        {
            this.values = rsv;
            this.M1 = ma1;
            this.M2 = ma2;
        }

        public override void OnInit()
        {
            if (values.Count > 0) {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        protected override StochasticOscillator Calculate(int index)
        {
            int offset = Period - 1;
            int mashortOffset = M1 - 1;
            int malongOffset = M2 - 1;
            int shortskip = offset + mashortOffset;
            int longskip = offset + malongOffset;

            if (index < shortskip) return new StochasticOscillator();

            float kvalue = values.Skip(index - mashortOffset).Take(M1).Average(item => item.Value);

            if (index < longskip) return new StochasticOscillator(kvalue);

            float dvalue = (this.Skip(index - malongOffset).Take(malongOffset).Sum(item => item.KValue.Value) + kvalue) / M2;

            return new StochasticOscillator(kvalue, dvalue);
        }

        public static StochasticOscillatorCollection Get(MarketDataCollectionFactory factory, int period = 9, int ma1 = 1, int ma2 = 3)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                    { "ma1", ma1 },
                    { "ma2", ma2 },
                }, 
                () => {
                return new StochasticOscillatorCollection(RawStochasticValueCollection.Get(factory, period), ma1, ma2);
            });
        }
    }
}
