﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public interface IExtMarketDataCollection
    {
        void OnInit();

        void OnAddOrUpdate(int index);
    }
}
