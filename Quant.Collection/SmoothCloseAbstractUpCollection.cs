﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Quant.Collection
{
    public class SmoothCloseAbstractUpCollection : ExtMarketDataCollection<float?>
    {
        public int Period { get; private set; }

        private CloseAbstractUpCollection values;

        private SmoothCloseAbstractUpCollection(CloseAbstractUpCollection values, int period) {
            this.values = values;
            this.Period = period;
        }

        protected override float? Calculate(int index)
        {
            int offset = Period - 1;
            if (index <= offset) return null;
            if (index == Period) return this.values.Skip(index - Period + 1).Take(Period).Average(item => item.GetValueOrDefault());

            float last = this[index - 1].Value;
            return last + (values[index] - last) / 6f;
        }

        public override void OnInit()
        {
            if (values.Count > 0)
            {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        public static SmoothCloseAbstractUpCollection Get(MarketDataCollectionFactory factory, int period)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new SmoothCloseAbstractUpCollection(CloseAbstractUpCollection.Get(factory), period);
            });
        }
    }
}
