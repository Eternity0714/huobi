﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public class CloseAbstractDownCollection : ExtMarketDataCollection<float?>
    {
        private CloseUpDownCollection values;

        private CloseAbstractDownCollection(CloseUpDownCollection values) {
            this.values = values;
        }

        public override void OnInit()
        {
            if (values.Count > 0)
            {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            var value = values[index];
            return value.HasValue ? -Math.Min(value.Value, 0) : value;
        }

        public static CloseAbstractDownCollection Get(MarketDataCollectionFactory factory)
        {
            return factory.GetCollection(null, () => {
                return new CloseAbstractDownCollection(CloseUpDownCollection.Get(factory));
            });
        }
    }
}
