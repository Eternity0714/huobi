﻿using Quant.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    /// <summary>
    /// BOLL指标
    /// </summary>
    public class BollingerCollection : ExtMarketDataCollection<Bollinger>
    {
        public int Period => values.Period;
        public float Weight { get; private set; }

        private CloseMovingAverageCollection values;
        private KlineCollection klines;

        private BollingerCollection(KlineCollection klines, CloseMovingAverageCollection values, float weight = 2f)
        {
            this.klines = klines;
            this.values = values;
            this.Weight = weight;
        }

        public override void OnInit()
        {
            if (values.Count > 0) {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        protected override Bollinger Calculate(int index)
        {
            if (!values[index].HasValue) return new Bollinger(null, null, null);

            var ma = values[index].Value;
            var items = klines.Skip(index - Period + 1).Take(Period);
            var sum = items.Sum(item => Math.Pow(item.Close - ma, 2));
            var md = Math.Sqrt(sum / Period);
            return new Bollinger(ma, ma + Weight * md, ma - Weight * md);
        }

        public static BollingerCollection Get(MarketDataCollectionFactory factory, int period = 20, float weight = 2f)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                    { "weight", weight },
                }, 
                () => {
                return new BollingerCollection(factory.Klines, CloseMovingAverageCollection.Get(factory, period), weight);
            });
        }
    }
}
