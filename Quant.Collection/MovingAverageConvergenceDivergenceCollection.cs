﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    /// <summary>
    /// MACD指标
    /// </summary>
    public class MovingAverageConvergenceDivergenceCollection : ExtMarketDataCollection<float?>
    {
        public int DifPeriod => Dif.DifPeriod;

        public int FastPeriod => Dif.FastPeriod;

        public int SlowPeriod => Dif.SlowPeriod;

        public MacdDifCollection Dif { get; private set; }

        public MacdDeaCollection Dea { get; private set; }

        private MovingAverageConvergenceDivergenceCollection(MacdDifCollection difs, MacdDeaCollection deas)
        {
            this.Dif = difs;
            this.Dea = deas;
        }

        public override void OnInit()
        {
            if (Dea.Count > 0) {
                base.OnAddOrUpdate(Dea.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            float? dif = Dif[index];
            float? dea = Dea[index];

            return dif.HasValue && dea.HasValue ? dif.Value - dea.Value : default(float?);
        }

        public static MovingAverageConvergenceDivergenceCollection Get(MarketDataCollectionFactory factory, int difPeriod = 9, int fastPeriod = 12, int slowPeriod = 26)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "difPeriod", difPeriod },
                    { "fastPeriod", fastPeriod },
                    { "slowPeriod", slowPeriod },
                }, 
                () => {
                return new MovingAverageConvergenceDivergenceCollection(
                    MacdDifCollection.Get(factory, difPeriod, fastPeriod, slowPeriod),
                    MacdDeaCollection.Get(factory, difPeriod, fastPeriod, slowPeriod)
                 );
            });
        }
    }
}
