﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public class RelativeStrengthIndexCollection : ExtMarketDataCollection<float?>
    {
        public int Period => ups.Period;

        private SmoothCloseAbstractUpCollection ups;
        private SmoothCloseAbstractDownCollection downs;

        private RelativeStrengthIndexCollection(SmoothCloseAbstractUpCollection ups, SmoothCloseAbstractDownCollection downs) {
            this.ups = ups;
            this.downs = downs;
        }

        public override void OnInit()
        {
            if (downs.Count > 0) {
                base.OnAddOrUpdate(downs.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            if (!ups[index].HasValue || !downs[index].HasValue) return null;
            var up = ups[index].Value;
            var down = downs[index].Value;
            return up / (up + down) * 100f;
        }

        public static RelativeStrengthIndexCollection Get(MarketDataCollectionFactory factory, int period)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new RelativeStrengthIndexCollection(SmoothCloseAbstractUpCollection.Get(factory, period), SmoothCloseAbstractDownCollection.Get(factory, period));
            });
        }
    }
}
