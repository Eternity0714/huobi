﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    /// <summary>
    /// 收盘价涨跌幅集合
    /// </summary>
    public class CloseUpDownCollection : ExtMarketDataCollection<float?>
    {
        private KlineCollection klines;

        private CloseUpDownCollection(KlineCollection klines) {
            this.klines = klines;
        }

        public override void OnInit()
        {
            if (klines.Count > 0) {
                base.OnAddOrUpdate(klines.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            if (index == 0) return null;
            return klines[index].Close - klines[index - 1].Close;
        }

        public static CloseUpDownCollection Get(MarketDataCollectionFactory factory) {
            return factory.GetCollection(null, () => {
                return new CloseUpDownCollection(factory.Klines);
            });
        }
    }
}
