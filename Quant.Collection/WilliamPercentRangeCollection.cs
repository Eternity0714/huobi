﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    /// <summary>
    /// WR指标
    /// </summary>
    public class WilliamPercentRangeCollection : ExtMarketDataCollection<float?>
    {
        private KlineCollection klines;

        public int Period { get; private set; }

        private WilliamPercentRangeCollection(KlineCollection klines, int period)
        {
            this.klines = klines;
            this.Period = period;
        }

        public override void OnInit()
        {
            if (klines.Count > 0) {
                base.OnAddOrUpdate(klines.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            int offset = Period - 1;
            if (index <= offset) return null;

            int skip = index - offset;
            float close = klines[index].Close;
            float high = this.klines.Skip(skip).Take(Period).Max(item => item.High);
            float low = this.klines.Skip(skip).Take(Period).Min(item => item.Low);
            return (high - close) / (high - low) * 100f;
        }

        public static WilliamPercentRangeCollection Get(MarketDataCollectionFactory factory, int period = 14)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "period", period },
                }, 
                () => {
                return new WilliamPercentRangeCollection(factory.Klines, period);
            });
        }
    }
}
