﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Collection
{
    public class MacdDeaCollection : ExtMarketDataCollection<float?>
    {
        private MacdDifCollection values;

        public int DifPeriod => values.DifPeriod;

        public int FastPeriod => values.FastPeriod;

        public int SlowPeriod => values.SlowPeriod;

        private MacdDeaCollection(MacdDifCollection values)
        {
            this.values = values;
        }

        public override void OnInit()
        {
            if (values.Count > 0) {
                base.OnAddOrUpdate(values.Count - 1);
            }
        }

        protected override float? Calculate(int index)
        {
            int offset = SlowPeriod + DifPeriod - 2;

            if (index < offset) return null;

            if (index == offset) {
                var value = values.Skip(SlowPeriod - 1).Take(DifPeriod).Average();
                return value;
            }

            float weight = 2f / (1 + DifPeriod);
            var lastdea = datas[index - 1].Value;
            return datas[index - 1].Value * (1f - weight) + values[index].Value * weight;
        }

        public static MacdDeaCollection Get(MarketDataCollectionFactory factory, int difPeriod = 9, int fastPeriod = 12, int slowPeriod = 26)
        {
            return factory.GetCollection(
                new Dictionary<string, object>() {
                    { "difPeriod", difPeriod },
                    { "fastPeriod", fastPeriod },
                    { "slowPeriod", slowPeriod },
                }, 
                () => {
                return new MacdDeaCollection(MacdDifCollection.Get(factory, difPeriod, fastPeriod, slowPeriod));
            });
        }
    }
}
