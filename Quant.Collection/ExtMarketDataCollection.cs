﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Collection
{
    public abstract class ExtMarketDataCollection<T> : MarketDataCollection<T>, IExtMarketDataCollection
    {
        protected abstract T Calculate(int index);

        public abstract void OnInit();

        public void OnAddOrUpdate(int index)
        {
            for (int i = this.Count; i <= index; i++)
            {
                T value = Calculate(i);
                if (i == this.Count - 1)
                {
                    this.datas[i] = value;
                }
                else
                {
                    this.datas.Add(value);
                }
            }
        }
    }
}
