﻿using System;

namespace Quant.Data
{
    public class CandleChart
    {
        public virtual DateTime OpenTime
        {
            get;
            set;
        }

        public float Amount
        {
            get;
            set;
        }

        public long Count
        {
            get;
            set;
        }

        public float Open
        {
            get;
            set;
        }

        public float Close
        {
            get;
            set;
        }

        public float Low
        {
            get;
            set;
        }

        public float High
        {
            get;
            set;
        }

        public float Volume
        {
            get;
            set;
        }
    }
}
