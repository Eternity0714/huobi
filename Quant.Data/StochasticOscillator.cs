﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data
{
    public class StochasticOscillator
    {
        public StochasticOscillator() { }

        public StochasticOscillator(float k)
        {
            KValue = k;
        }

        public StochasticOscillator(float k, float d) {
            KValue = k;
            DValue = d;
        }

        public float? KValue { get; private set; }
        public float? DValue { get; private set; }
        public float? JValue
        {
            get
            {
                if (KValue.HasValue && DValue.HasValue)
                {
                    return 3f * KValue.Value - 2f * DValue.Value;
                }
                return null;
            }
        }
    }
}
