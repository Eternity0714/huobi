﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Data
{
    public class Bollinger
    {
        public Bollinger() { }

        public Bollinger(double? mb, double? ub, double? lb) {
            this.Middle = mb;
            this.Upper = ub;
            this.Lower = lb;
        }

        /// <summary>
        /// 中轨线
        /// </summary>
        public double? Middle { get; private set; }
        /// <summary>
        /// 上轨线
        /// </summary>
        public double? Upper { get; private set; }
        /// <summary>
        /// 下轨线
        /// </summary>
        public double? Lower { get; private set; }
    }
}
