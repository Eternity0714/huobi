﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quant.Core
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> ts) {
            return ts == null || ts.Count() == 0;
        }
    }
}
