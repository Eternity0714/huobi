﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Core
{
    public static class Mathf
    {
        public static int Clamp(int value, int min, int max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static float Clamp(float value, float min, float max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static sbyte Clamp(sbyte value, sbyte min, sbyte max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static ulong Clamp(ulong value, ulong min, ulong max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static uint Clamp(uint value, uint min, uint max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static long Clamp(long value, long min, long max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static ushort Clamp(ushort value, ushort min, ushort max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static short Clamp(short value, short min, short max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static double Clamp(double value, double min, double max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static decimal Clamp(decimal value, decimal min, decimal max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static byte Clamp(byte value, byte min, byte max)
        {
            value = Math.Max(min, value);
            value = Math.Min(max, value);
            return value;
        }

        public static DateTime Clamp(DateTime value, DateTime min, DateTime max)
        {
            long ticks = Math.Max(min.Ticks, value.Ticks);
            ticks = Math.Min(max.Ticks, ticks);
            return new DateTime(ticks, value.Kind);
        }
    }
}
