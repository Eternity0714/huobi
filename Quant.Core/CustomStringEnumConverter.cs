﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Newtonsoft.Json
{

    public class CustomStringEnumConverter<T> : JsonConverter<T> where T : struct, Enum
    {
        private IReadOnlyDictionary<T, string> nicknames;

        public CustomStringEnumConverter() : this(new Dictionary<T, string>()) { }

        public CustomStringEnumConverter(IReadOnlyDictionary<T, string> nicknames)
        {
            this.nicknames = nicknames;
        }


        public override T ReadJson(JsonReader reader, Type objectType, T existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            T code;
            if (reader.TokenType == JsonToken.String)
            {
                string name = reader.Value.ToString();
                if (Enum.TryParse(name, out code))
                {
                    return code;
                }

                if (nicknames.Values.Contains(name))
                {
                    code = nicknames.FirstOrDefault(item => item.Value == name).Key;
                    return code;
                }

                throw new Exception($"枚举 '{objectType}' 中找不到值: {name}");
            }
            else if (reader.TokenType == JsonToken.Integer)
            {
                var v = reader.ReadAsInt32();

                foreach (T t in Enum.GetValues(typeof(T)))
                {
                    if (t.GetHashCode() == v)
                    {
                        return t;
                    }
                }

                throw new Exception($"枚举 '{objectType}' 中找不到值: {v}");
            }

            throw new Exception($"无法解析的值: {reader}");
        }

        public override void WriteJson(JsonWriter writer, T value, JsonSerializer serializer)
        {
            if (nicknames.TryGetValue(value, out string name))
            {
                writer.WriteValue(name);
            }
            else
            {
                writer.WriteValue(value.ToString());
            }
        }
    }
}