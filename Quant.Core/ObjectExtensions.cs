﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Core
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object obj) {
            return obj == null;
        }

        public static void ThrowIfIsNull(this object obj, string name) {
            if (obj.IsNull()) throw new ArgumentNullException(name);
        }
    }
}
