﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Newtonsoft.Json
{
    /// <summary>
    /// 动态属性转换约定
    /// </summary>
    /// <remarks>
    /// 处理场景 树形结构数据 后台代码实体定义 为 Id Childrens 但是前台树形控件所需数据结构为  id,nodes
    /// 这个时候可以使用该属性约定转换类 动态设置 序列化后字段名称
    /// </remarks>
    /// <example>
    ///     JsonSerializerSettings setting = new JsonSerializerSettings();
    ///     setting.ContractResolver = new PropsContractResolver(new Dictionary<string, string> { { "Id", "id" }, { "Text", "text" }, { "Childrens", "nodes" } });
    ///     string AA = JsonConvert.SerializeObject(cc, Formatting.Indented, setting);
    public class PropsContractResolver : DefaultContractResolver
    {
        private IReadOnlyDictionary<string, string> dict_props = null;

        public PropsContractResolver(IReadOnlyDictionary<string, string> dictPropertyName) {
            this.dict_props = dictPropertyName;
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            if (dict_props != null && dict_props.TryGetValue(propertyName, out string newPropertyName)) {
                return newPropertyName;
            }
            return base.ResolvePropertyName(propertyName);
        }
    }
}
