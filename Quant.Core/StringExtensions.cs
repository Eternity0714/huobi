﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quant.Core
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string str) {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static void ThrowIfNullOrEmpty(this string str, string name) {
            if (str.IsNullOrEmpty()) throw new ArgumentNullException(name);
        }

        public static void ThrowIfNullOrWhiteSpace(this string str, string name)
        {
            if (str.IsNullOrWhiteSpace()) throw new ArgumentNullException(name);
        }
    }
}
