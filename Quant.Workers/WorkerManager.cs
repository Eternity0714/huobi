﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Quant.Workers
{
    public class WorkerManager
    {
        IList<WorkerEntity> entities = new List<WorkerEntity>();

        object _lock = new object();

        public void Add<TWorker>() where TWorker : Worker
        {
            lock (_lock) {
                if (entities.Any(item => item.Worker.GetType() == typeof(TWorker)))
                {
                    throw new Exception();
                }
                Worker worker = Activator.CreateInstance<TWorker>();
                WorkerEntity entity = new WorkerEntity(worker);
                entities.Add(entity);
            }
        }

        public void Remove(string name) {
            lock (_lock) {
                var entity = entities.FirstOrDefault(item => item.Name == name);
                if (entity == null) return;
                entities.Remove(entity);
                entity.Token.Cancel();
            }
        }

        public void Remove<TWorker>() where TWorker : Worker {
            lock (_lock) {
                var entity = entities.FirstOrDefault(item => item.Worker.GetType() == typeof(TWorker));
                if (entity == null) return;
                entities.Remove(entity);
                entity.Token.Cancel();
            }
        }

        public void CheckDone() {
            lock (_lock) {
                for (int i = entities.Count - 1; i >= 0; --i) {
                    var entity = entities[i];
                    if (entity.Token.IsCancellationRequested) {
                        entities.RemoveAt(i);
                    }
                }
            }
        }

        public bool IsEmpty() {
            return entities.Count == 0;
        }

        private class WorkerEntity {
            public string Name => Worker.GetType().Name;

            internal Worker Worker;

            public CancellationTokenSource Token { get; private set; }

            private Task task;

            public WorkerEntity(Worker worker)
            {
                this.Worker = worker;
                Token = new CancellationTokenSource();
                task = Task.Factory.StartNew(async (state) =>
                {
                    try
                    {
                        CancellationToken token = (CancellationToken)state;
                        await Worker.Init();
                        while (!Token.Token.IsCancellationRequested && await Worker.Update()) ;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw;
                    }
                    finally {
                        await Worker.Destroy();
                    }
                }, Token.Token, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);

            }
        }
    }
}
