﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Quant.Workers
{
    public abstract class Worker
    {
        internal async Task Init() {
            await OnInit();
        }

        protected abstract Task OnInit();

        internal async Task<bool> Update() {
            return await OnUpdate();
        }

        protected abstract Task<bool> OnUpdate();

        internal async Task Destroy() {
            await OnDestroy();
        }

        protected abstract Task OnDestroy();
    }
}
