﻿using System;
using System.Threading.Tasks;
using Quant.Collection;
using Quant.Workers;

namespace Quant.Data.Huobi.Daemon
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkerManager workerManager = new WorkerManager();
            workerManager.Add<CustomWorker>();

            while (!workerManager.IsEmpty()) workerManager.CheckDone();

            Console.ReadKey();
        }
    }

    public class CustomWorker : Worker
    {
        MarketWsProvider market;
        MarketDataCollectionFactory factory;
        HuobiHttpProvider http;
        MovingAverageConvergenceDivergenceCollection macdCollection;

        int i = 0;

        float cash = 100000f;
        float coin = 0f;

        float lastOrderMacd = 0f;

        protected override async Task OnInit()
        {
            http = new HuobiHttpProvider(HuobiHttpProvider.ACCESS_KEY, HuobiHttpProvider.SECRET_KEY);
            factory = new MarketDataCollectionFactory();
            var datas = await http.GetKLineAsync("btcusdt", HuobiPeriod.Minute_5, 2000);

            for (int i = 0; i < datas.Length; i++)
            {
                factory.AddOrUpdateKlineData(datas[i]);
            }

            //market = new MarketWsProvider();
            //await market.AddKlineListener("btcusdt", HuobiPeriod.Minute_5, OnKlineGet);

            macdCollection = MovingAverageConvergenceDivergenceCollection.Get(factory);
        }

        protected override async Task<bool> OnUpdate()
        {
            if (macdCollection.Count <= i) return false;

            bool canBuy = CanBuy(i);
            bool canSell = CanSell(i);

            var kline = factory.Klines[i];
            var macd = macdCollection[i];

            if (canBuy && cash > 0f)
            {
                coin = cash / kline.Close * 0.998f;
                cash = 0f;
                lastOrderMacd = macd.Value;
            }
            else if (canSell && coin > 0f) {
                cash = coin * kline.Close * 0.998f;
                coin = 0f;
                lastOrderMacd = macd.Value;
            }

            Console.WriteLine($@"{kline.OpenTime}, {kline.Close}, {(macd.HasValue ? macd.Value.ToString() : "null")}{(canBuy ? ",买" : "")}{(canSell ? ",卖" : "")}{(canBuy || canSell ? $", {cash}, {coin}" : "")}");

            ++i;
            return true;
        }

        protected override async Task OnDestroy()
        {
            
        }

        //private void OnKlineGet(CandleChart data) {
        //    factory.AddOrUpdateKlineData(data);
        //}

        private bool CanBuy(int index) {
            if (index < 2) return false;
            if (macdCollection.Count <= index) return false;

            var m = macdCollection[index];
            if (!m.HasValue) return false;

            var last1 = macdCollection[index - 1];
            if (!last1.HasValue) return false;

            var last2 = macdCollection[index - 2];
            if (!last2.HasValue) return false;

            return m.Value > last1.Value && last1.Value < last2.Value;
        }

        private bool CanSell(int index) {
            if (index < 2) return false;
            if (macdCollection.Count <= index) return false;

            var m = macdCollection[index];
            if (!m.HasValue) return false;

            var last1 = macdCollection[index - 1];
            if (!last1.HasValue) return false;

            var last2 = macdCollection[index - 2];
            if (!last2.HasValue) return false;

            return m.Value < last1.Value && last1.Value > last2.Value;
        }
    }
}
